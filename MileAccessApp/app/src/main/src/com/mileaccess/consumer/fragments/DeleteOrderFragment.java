package com.mileaccess.consumer.fragments;

import java.util.Date;

import com.android.mileaccess.R;
import com.android.mileaccess.R.layout;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class DeleteOrderFragment extends BaseFragment{
	
	AutoCompleteTextView orderNumber;
	Button btnDelete;
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		context = getActivity();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
	
		// Retrieving the currently selected item number
		int position = getArguments().getInt("position");
		
		// List of rivers
		//String[] countries = getResources().getStringArray(R.array.menuOptionsConsumer);
		
		// Creating view correspoding to the fragment
		View v = inflater.inflate(R.layout.fragment_track_order, container, false);
		
		// Getting reference to the TextView of the Fragment
		//TextView tv = (TextView) v.findViewById(R.id.tv_content);
		
		// Setting currently selected river name in the TextView
		//tv.setText(countries[position]);
		//new DatePickerDialog1964(context).show();

		initializeScreen(v);
		return v;
	}
	
	
	private void initializeScreen(View view) {
        orderNumber = (AutoCompleteTextView) view.findViewById(R.id.orderno);
        btnDelete = (Button) view.findViewById(R.id.track_button);

        btnDelete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
               
            }
        });		
	}	
}