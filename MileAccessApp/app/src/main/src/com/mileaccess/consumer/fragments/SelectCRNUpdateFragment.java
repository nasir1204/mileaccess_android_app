package com.mileaccess.consumer.fragments;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.message.BasicNameValuePair;

import com.android.mileaccess.R;
import com.mileaccess.consumer.HomeActivity;
import com.mileaccess.consumer.controllers.HomeController;
import com.mileaccess.consumer.utilities.ExpandableListAdapter;
import com.mileaccess.consumer.utilities.PreferencesManager;
import com.mileaccess.consumer.utilities.PreferencesManagerConstants;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.TableLayout.LayoutParams;

public class SelectCRNUpdateFragment extends BaseFragment{

	AutoCompleteTextView orderNumber;
	Button btnTrack;
	private OrderDetailsTask orderDetailsTask;
	private OrderListTask orderListTask;
	private TableLayout tv;
	private HomeController homeController;
	private Cookie orderListCookie;
	ExpandableListAdapter listAdapter;
	ExpandableListView expListView;
	List<String> listDataHeader;
	List<String> listStatusHeader;
	HashMap<String, List<String>> listDataChild;
	List<String> crnList = null;
	//String[] crnList;
	//String[] statusList;
	List<String> statusList = null;
	List<String> orderDetailsList;
	List<Cookie> cookieList;
	HomeActivity mainActivity;   
	private ProgressBar spinner;
    TextView noOrders;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		context = activity;
		mainActivity = (HomeActivity) activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.fragment_home, container, false);
		initializeScreen(v);
		return v;
	}
	
	@Override
	public void onPause()
	{
		super.onPause();
	}
	
	
	@Override
	public void onDestroy()
	{
		super.onDestroy();
		mainActivity.dbHandler.deleteAllContact();
	}


	private void initializeScreen(View view) {
		//tv=(TableLayout) view.findViewById(R.id.table);
		  homeController = new HomeController(context);
	      spinner=(ProgressBar)view.findViewById(R.id.progressBar);
	      spinner.setVisibility(View.GONE);

	      noOrders = (TextView) view.findViewById(R.id.textNoOrder);
	      noOrders.setVisibility(View.GONE);

		if(PreferencesManager.getBool(context, PreferencesManagerConstants.IS_USER_LOGGED_IN, false))
		{
			orderListTask = new OrderListTask();
			orderListTask.execute((Void) null);
		    spinner.setVisibility(View.VISIBLE);
		}



		// get the listview
		expListView = (ExpandableListView) view.findViewById(R.id.lvExp);

		listDataHeader = new ArrayList<String>();
		listStatusHeader = new ArrayList<String>();
		listDataChild = new HashMap<String, List<String>>();


		expListView.setOnGroupClickListener(new OnGroupClickListener() {

			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
//				 Toast.makeText(context,
//				 "Group Clicked " + listDataHeader.get(groupPosition),
//				 Toast.LENGTH_SHORT).show();
				String crn = listDataHeader.get(groupPosition);
				Bundle args = new Bundle();
				args.putString("crn", crn);
				Fragment fragment = null;
				fragment = new UpdateOrderFragment();
				fragment.setArguments(args);
				FragmentManager fragmentManager = mainActivity.getSupportFragmentManager();
				fragmentManager.beginTransaction()
				.replace(R.id.content_frame, fragment)
				.commit();
		        return false;
			}
		});

		// Listview Group expanded listener
		expListView.setOnGroupExpandListener(new OnGroupExpandListener() {

			@Override
			public void onGroupExpand(int groupPosition) {
//				Toast.makeText(context,
//						listDataHeader.get(groupPosition) + " Expanded",
//						Toast.LENGTH_SHORT).show();

			}
		});

		// Listview Group collasped listener
		expListView.setOnGroupCollapseListener(new OnGroupCollapseListener() {

			@Override
			public void onGroupCollapse(int groupPosition) {


			}
		});

		// Listview on child click listener
		expListView.setOnChildClickListener(new OnChildClickListener() {

			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
				// TODO Auto-generated method stub

				return false;
			}
		});
	}	


	
	public class OrderDetailsTask extends AsyncTask<Void, Void, Boolean> 
	{

		String crn;
		int position;
		List<Cookie> orderDetails = null;
		OrderDetailsTask(int groupPostion,String crnValue) {
			position = groupPostion;
			crn = crnValue;
		}

		@SuppressWarnings("deprecation")
		@Override
		protected Boolean doInBackground(Void... params) {
			//tv.removeAllViewsInLayout();
			try {
				//stuff that updates ui
				//http://stackoverflow.com/questions/4968226/show-data-in-table-view-in-android
				//homeController.fetchPreviousOrderList();
				BasicCookieStore cookieStore = null;
				try {
					List <NameValuePair> nvps = new ArrayList <NameValuePair>();
	    	        nvps.add(new BasicNameValuePair("order", crn));
					cookieStore = homeController.fetchDetailedOrder(nvps);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				cookieList = cookieStore.getCookies();
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						int i = 0;
						for(i = 0; i < cookieList.size();i++)
						{
							orderListCookie = cookieList.get(i);
							if((orderListCookie.getName()).equals("order_details"))
							{
								String orderDetails = orderListCookie.getValue();
								mainActivity.dbHandler.addOrderDetails(crn, orderDetails);
								orderDetailsList = new ArrayList<String>(Arrays.asList(orderDetails.split("\\|")));

//								for(int k = 0; i < orderDetailsList.size();i++)
//								{
//									if(k==0)
//									{
//										orderDetailsList.add(k, "CRN Number : "+orderDetailsList.get(k));
//									}
//								}
							}
						}		

					    listDataChild.put(listDataHeader.get(position), orderDetailsList);
						
						listAdapter = new ExpandableListAdapter(context,crnList,statusList,listDataChild);
						expListView.setAdapter(listAdapter);
				    	expListView.expandGroup(position);
					}
				});
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;

		}

		@Override
		protected void onPostExecute(final Boolean success) {
			orderDetailsTask = null;
			//stop the progress spinner
			if (success) {
				//  login success and move to main Activity here.
			} else {
				// login failure
				Toast.makeText(context,"Incorrect Details. Unable to create profile.", Toast.LENGTH_LONG).show();
			}

		}
		@Override
		protected void onCancelled() {
			orderDetailsTask = null;
		}
	}

	public class OrderListTask extends AsyncTask<Void, Void, Boolean> 
	{
		List<Cookie> orderDetails = null;
		OrderListTask() {

		}

		@SuppressWarnings("deprecation")
		@Override
		protected Boolean doInBackground(Void... params) {
			//tv.removeAllViewsInLayout();
			try {
				//stuff that updates ui
				//http://stackoverflow.com/questions/4968226/show-data-in-table-view-in-android
				//homeController.fetchPreviousOrderList();
				BasicCookieStore cookieStore = null;
				try {
					cookieStore = homeController.fetchPreviousOrderList();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				finally
				{
				      
				}
				cookieList = cookieStore.getCookies();
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						spinner.setVisibility(View.GONE);
						for(int i = 0; i < cookieList.size();i++)
						{
							orderListCookie = cookieList.get(i);
							if((orderListCookie.getName()).equals("order_ids"))
							{
								String crns = orderListCookie.getValue();
								if(!TextUtils.isEmpty(crns))
								{
									crnList = new ArrayList<String>(Arrays.asList(crns.split("\\|")));
									//crnList = crns.split("\\|");
									int a = crnList.size();
									for (int j = 0; j < crnList.size(); j++) {
										System.out.println(crnList.get(j));
										listDataHeader.add(crnList.get(j));
									}
								}
								else
								{
									Toast.makeText(context,"No ordes", Toast.LENGTH_LONG).show();

								      noOrders.setVisibility(View.VISIBLE);
								}
							}
							if((orderListCookie.getName()).equals("order_status"))
							{
								String status = orderListCookie.getValue();
								if(!TextUtils.isEmpty(status))
								{
									statusList = new ArrayList<String>(Arrays.asList(status.split("\\|")));
									//statusList = status.split("|");
									int a = crnList.size();
									for (int j = 0; j < statusList.size(); j++) {
										System.out.println(statusList.get(j));
										listDataHeader.add(statusList.get(j));
									}
								}
							}
						}		

						List<String> top250 = new ArrayList<String>();
						top250.add("  Fetching Details ... Please wait. ");
						if(crnList!=null)
						{
						int k = 0;	
						for(k = 0; k < crnList.size();k++)
						{
							listDataChild.put(listDataHeader.get(k), top250);
						}
						listAdapter = new ExpandableListAdapter(context,crnList,statusList,listDataChild);
						expListView.setAdapter(listAdapter);
						
						}
					}
				});
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			orderDetailsTask = null;
			//stop the progress spinner
			if (success) {
				//  login success and move to main Activity here.
			} else {
				// login failure
				Toast.makeText(context,"Incorrect Details. Unable to create profile.", Toast.LENGTH_LONG).show();
			}
		}
		@Override
		protected void onCancelled() {
			orderDetailsTask = null;
		}
	}
}