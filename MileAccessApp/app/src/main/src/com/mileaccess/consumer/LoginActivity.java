package com.mileaccess.consumer;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Paint;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.text.util.Linkify;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.cookie.Cookie;
import org.apache.http.message.BasicNameValuePair;

import com.android.mileaccess.R;
import com.mileaccess.consumer.controllers.CommonController;
import com.mileaccess.consumer.controllers.LoginController;
import com.mileaccess.consumer.utilities.PreferencesManager;
import com.mileaccess.consumer.utilities.PreferencesManagerConstants;



/**
 * Android login screen Activity
 */
public class LoginActivity extends Activity implements LoaderCallbacks<Cursor> {

	private static final String LOGIN_URL = "http://www.mileaccess.com/login.html";

	private UserLoginTask userLoginTask = null;
	private View loginFormView;
	private View progressView;
	private AutoCompleteTextView emailTextView;
	private EditText passwordTextView;
	private TextView signUpTextView;
	private TextView forgotPasswordTextView;
	private CommonController commonController;
	private String role = null;
	public boolean newUserFlag = false;
	List<Cookie> cookies;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
/*
		if(PreferencesManager.getBool(this, PreferencesManagerConstants.IS_USER_LOGGED_IN, false))
		{
			Intent intent = new Intent(getApplicationContext(),
					HomeActivity.class);
			if (intent != null) {
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				getApplicationContext().startActivity(intent);
			}
		}
		
*/		      
		//  http://stackoverflow.com/questions/8500283/how-to-hide-action-bar-before-activity-is-created-and-then-show-it-again
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
				WindowManager.LayoutParams.FLAG_FULLSCREEN);  
		
		setContentView(R.layout.activity_login);

		emailTextView = (AutoCompleteTextView) findViewById(R.id.email);
		loadAutoComplete();

		commonController = new CommonController(LoginActivity.this);

		forgotPasswordTextView = (TextView) findViewById(R.id.forgot_password);
		forgotPasswordTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) 
			{
				Intent intent = new Intent(getApplicationContext(),
						RecoverPasswordActivity.class);
				if (intent != null) {
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
					getApplicationContext().startActivity(intent);
				}
			}
		});
		passwordTextView = (EditText) findViewById(R.id.password);
		passwordTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
				if (id == EditorInfo.IME_NULL) {
					initLogin();
					return true;
				}
				return false;
			}
		});

		Button loginButton = (Button) findViewById(R.id.email_sign_in_button);
		loginButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				hideKeyboard();
				initLogin();
			}
		});

		loginFormView = findViewById(R.id.login_form);
		progressView = findViewById(R.id.login_progress);

		//adding underline and link to signup textview
		signUpTextView = (TextView) findViewById(R.id.signUpTextView);
		signUpTextView.setPaintFlags(signUpTextView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
		Linkify.addLinks(signUpTextView, Linkify.ALL);

		signUpTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.i("LoginActivity", "Sign Up Activity activated.");
				// this is where you should start the signup Activity
				LoginActivity.this.startActivity(new Intent(LoginActivity.this, SigninAcitvity.class));
			}
		});
	}

	private void hideKeyboard() {   
	    // Check if no view has focus:
	    View view = this.getCurrentFocus();
	    if (view != null) {
	        InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
	        inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	    }
	}
	
	private void loadAutoComplete() {
		getLoaderManager().initLoader(0, null, this);
	}
	
	@Override
	public void onBackPressed() {
		finish();
	}
	
	/**
	 * Validate Login form and authenticate.
	 */
	public void initLogin() {
		if (userLoginTask != null) {
			return;
		}

		emailTextView.setError(null);
		passwordTextView.setError(null);

		String email = emailTextView.getText().toString();
		String password = passwordTextView.getText().toString();

		boolean cancelLogin = false;
		View focusView = null;

		if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
			passwordTextView.setError(getString(R.string.invalid_password));
			focusView = passwordTextView;
			cancelLogin = true;
		}

		if (TextUtils.isEmpty(email)) {
			emailTextView.setError(getString(R.string.field_required));
			focusView = emailTextView;
			cancelLogin = true;
		} else if (!isEmailValid(email)) {
			emailTextView.setError(getString(R.string.invalid_email));
			focusView = emailTextView;
			cancelLogin = true;
		}

		if (cancelLogin) {
			// error in login
			focusView.requestFocus();
		} else {
			// show progress spinner, and start background task to login
			showProgress(true);
			userLoginTask = new UserLoginTask(email, password);
			userLoginTask.execute((Void) null);
		}
	}

	private boolean isEmailValid(String email) {
		//add your own logic
		return email.contains("@");
	}

	private boolean isPasswordValid(String password) {
		//add your own logic
		return password.length() > 4;
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	public void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

			loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
			loginFormView.animate().setDuration(shortAnimTime).alpha(
					show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
						}
					});

			progressView.setVisibility(show ? View.VISIBLE : View.GONE);
			progressView.animate().setDuration(shortAnimTime).alpha(
					show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							progressView.setVisibility(show ? View.VISIBLE : View.GONE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			progressView.setVisibility(show ? View.VISIBLE : View.GONE);
			loginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}


	@Override
	public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
		return new CursorLoader(this,
				// Retrieve data rows for the device user's 'profile' contact.
				Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
						ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

						// Select only email addresses.
						ContactsContract.Contacts.Data.MIMETYPE +
						" = ?", new String[]{ContactsContract.CommonDataKinds.Email
			.CONTENT_ITEM_TYPE},

			// Show primary email addresses first. Note that there won't be
			// a primary email address if the user hasn't specified one.
			ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
	}

	@Override
	public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
		List<String> emails = new ArrayList<String>();
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			emails.add(cursor.getString(ProfileQuery.ADDRESS));
			cursor.moveToNext();
		}

		addEmailsToAutoComplete(emails);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> cursorLoader) {

	}

	private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
		//Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
		ArrayAdapter<String> adapter =
				new ArrayAdapter<String>(LoginActivity.this,
						android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

		emailTextView.setAdapter(adapter);
	}


	private interface ProfileQuery {
		String[] PROJECTION = {
				ContactsContract.CommonDataKinds.Email.ADDRESS,
				ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
		};

		int ADDRESS = 0;
		int IS_PRIMARY = 1;
	}

	/**
	 * Async Login Task to authenticate
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, Boolean> 
	{

		private final String emailStr;
		private final String passwordStr;

		UserLoginTask(String email, String password) {
			emailStr = email;
			passwordStr = password;
		}

		@SuppressWarnings("deprecation")
		@Override
		protected Boolean doInBackground(Void... params) {

			boolean flag = false;

			List <NameValuePair> nvps = new ArrayList <NameValuePair>();
			nvps.add(new BasicNameValuePair("email", emailStr));
			nvps.add(new BasicNameValuePair("pwd1", passwordStr));

			try {
				LoginController loginController = new LoginController(LoginActivity.this);
				List<Cookie> cookies = loginController.fetchCookie3(nvps);
				if(cookies.isEmpty() || cookies == null)
				{
					flag = false;
				}
				else
				{
					if((cookies.get(0).getName()).equals("+please+register"))
					{
						Toast.makeText(LoginActivity.this, "New User.Please Register!!", Toast.LENGTH_LONG).show();
						newUserFlag = true;
						return false;
					}
					Cookie loginCookie = cookies.get(1);
					if((loginCookie.getName()).equals("role"))
					{
						role = loginCookie.getValue();
					}
					PreferencesManager.setBool(LoginActivity.this, PreferencesManagerConstants.IS_USER_LOGGED_IN, true);
					PreferencesManager.setString(LoginActivity.this, PreferencesManagerConstants.PROFILE_TYPE, role);
					PreferencesManager.setString(LoginActivity.this, PreferencesManagerConstants.USER_EMAIL, emailStr);
					flag = true;
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return flag;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			userLoginTask = null;
			//stop the progress spinner
			showProgress(false);

			if (success) {
				//  login success and move to main Activity here.
				Intent intent = new Intent(LoginActivity.this,
						HomeActivity.class);
				if (intent != null) {
					intent.putExtra("profileType", role);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
					getApplicationContext().startActivity(intent);

					finish();
				}
			} else {
				// login failure
				if(!newUserFlag)
				{
					passwordTextView.setError(getString(R.string.incorrect_password));
					passwordTextView.requestFocus();
				}
				else
				{
					emailTextView.setText("");
					passwordTextView.setText("");
					emailTextView.requestFocus();
				}
			}


		}

		@Override
		protected void onCancelled() {
			userLoginTask = null;
			showProgress(false);
		}
	}
}