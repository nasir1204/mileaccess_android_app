package com.mileaccess.consumer.controllers;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import com.mileaccess.consumer.utilities.PreferencesManager;
import com.mileaccess.consumer.utilities.PreferencesManagerConstants;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class LoginController {
	
    protected Context context;
    public static List<Cookie> cookies;
    public static Cookie cookie1;
    public static Date date;
    
    
    public LoginController(Context context){
        this.context = context;
    }

       public List<Cookie> fetchCookie3(List <NameValuePair> nvps) throws IOException
       {
    	   
           DefaultHttpClient httpclient = new DefaultHttpClient();

           HttpPost httpost = new HttpPost("http://www.mileaccess.com/login.html");

        

           httpost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));

           HttpResponse response = httpclient.execute(httpost);
           HttpEntity entity = response.getEntity();

           System.out.println("Login form get: " + response.getStatusLine());
           if (entity != null) {
               entity.consumeContent();
           }

           System.out.println("Post logon cookies:");
            cookies = httpclient.getCookieStore().getCookies();
           if (cookies.isEmpty()) {
               System.out.println("None");
           } else {
               cookie1 = cookies.get(0);
               PreferencesManager.setLong(context, PreferencesManagerConstants.COOKIE_DATE, cookie1.getExpiryDate().getTime());
               for (int i = 0; i < cookies.size(); i++) {
                   System.out.println("- " + cookies.get(i).toString());
                  // date = cookies.get(i).toString().substring(cookies.get(i).toString().indexOf("expiry: "), cookies.get(i).toString().indexOf("],"));                 
               }
               //date = cookies.get(0).getExpiryDate();
           }
           httpclient.getConnectionManager().shutdown();    
           return cookies;
       }
	   
}
