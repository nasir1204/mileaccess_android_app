package com.mileaccess.consumer.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;

import com.mileaccess.consumer.LoginActivity;
import com.mileaccess.consumer.utilities.PreferencesManager;
import com.mileaccess.consumer.utilities.PreferencesManagerConstants;

import android.content.Context;

public class CommonController {

    protected Context context;

    public CommonController(Context context){
        this.context = context;
    }
    
    //CRN-15568bf2180311e51cc2
    @SuppressWarnings("deprecation")
	public BasicCookieStore fetchCookie(List <NameValuePair> nvps , String url) throws IOException
    {
    	    LoginController loginController = new LoginController(context);
            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpPost httpost = new HttpPost(url);
            Date expiryDate = new Date(PreferencesManager.getLong(context, PreferencesManagerConstants.COOKIE_DATE, 0));

       //     Cookie sessionCookie =  loginController.cookies;
            String userEmail = PreferencesManager.getString(context, PreferencesManagerConstants.USER_EMAIL, "");
            BasicCookieStore cookieStore = new BasicCookieStore();
            BasicClientCookie cookie = new BasicClientCookie("email", userEmail);
            cookie.setDomain(".mileaccess.com");
            cookie.setPath("/");
            cookie.setExpiryDate(expiryDate);
            cookieStore.addCookie(cookie);    
            HttpContext localContext = new BasicHttpContext();
            localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
            
           
            httpost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));

            HttpResponse response = httpclient.execute(httpost, localContext);

            HttpEntity entity = response.getEntity();

            System.out.println("Login form get: " + response.getStatusLine());
            if (entity != null) {
                entity.consumeContent();
            }

            System.out.println("Post logon cookies:");
            List<Cookie> cookies = httpclient.getCookieStore().getCookies();
            if (cookies.isEmpty()) {
                System.out.println("None");
            } else {
                for (int i = 0; i < cookies.size(); i++) {
                    System.out.println("- " + cookies.get(i).toString());
                }
            }
            httpclient.getConnectionManager().shutdown();
    		return cookieStore;    
          
    }
    
}
