package com.mileaccess.consumer.fragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.message.BasicNameValuePair;

import com.android.mileaccess.R;
import com.mileaccess.consumer.LoginActivity;
import com.mileaccess.consumer.controllers.CommonController;
import com.mileaccess.consumer.fragments.CreateOrderFragment.OrderCreationTask;
import com.mileaccess.consumer.utilities.PreferencesManager;
import com.mileaccess.consumer.utilities.PreferencesManagerConstants;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class UpdateWorkstatusFragment extends BaseFragment{
	
	Spinner statusSpinner;
	Button btnUpdate;
	EditText emailTextView,statusDateTextView,pincodeTextView;
	UpdateStatusTask updateStatusTask;
	CommonController commonController;
    ProgressDialog dialog;
	final String UPDATE_WORKSTATUS_URL = "http://mileaccess.com/update_workstatus.html";
	private FragmentActivity myContext;

	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		context = getActivity();
		myContext=(FragmentActivity) activity;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
	
		
		// Creating view correspoding to the fragment
		View v = inflater.inflate(R.layout.fragment_update_workstatus, container, false);

		initializeScreen(v);
		return v;
	}
	
	
	private void initializeScreen(View view) {
		statusSpinner = (Spinner) view.findViewById(R.id.servicetypeSpinner);
		emailTextView = (EditText) view.findViewById(R.id.email);
		statusDateTextView = (EditText) view.findViewById(R.id.pickupdate);
		pincodeTextView = (EditText) view.findViewById(R.id.pincode);
        btnUpdate = (Button) view.findViewById(R.id.update_button);

        btnUpdate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
				hideKeyboard(myContext);
            	initUpdateStatus();
            }
        });		
        
        statusDateTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				//new DatePickerDialog1964(context).show();
				showDatePicker();
			}
		});
        
        commonController = new CommonController(context);
        emailTextView.setText(PreferencesManager.getString(context, PreferencesManagerConstants.USER_EMAIL, ""));
	}	
	
	public void initUpdateStatus() {
			//servicetype
			String status = statusSpinner.getSelectedItem().toString();
			String email = emailTextView.getText().toString();
			String date = statusDateTextView.getText().toString();
			String pincode = pincodeTextView.getText().toString();
			
			boolean cancelReset = false;
			View focusView = null;
			
			if (TextUtils.isEmpty(email)) {
				emailTextView.setError(getString(R.string.field_required));
				focusView = emailTextView;
				cancelReset = true;
			}
			if (TextUtils.isEmpty(date)) {
				statusDateTextView.setError(getString(R.string.field_required));
				focusView = statusDateTextView;
				cancelReset = true;
			}
			if (TextUtils.isEmpty(pincode)) {
				pincodeTextView.setError(getString(R.string.field_required));
				focusView = pincodeTextView;
				cancelReset = true;
			}

			if (cancelReset) {
				// error in login
				focusView.requestFocus();
			} else {
				dialog = ProgressDialog.show(context, "", "Please wait...", true);
				updateStatusTask = new UpdateStatusTask(status,email,date,pincode);
				updateStatusTask.execute((Void) null);
			}
		}
	
	@Override
	public void onPause()
	{
		super.onPause();
	}

	public class UpdateStatusTask extends AsyncTask<Void, Void, Boolean> 
	{

		private final String statusStr;
		private final String emailStr;
		private final String dateStr;
		private final String pincodeStr;



		UpdateStatusTask(String status,String email,String date,String pincode) {
			statusStr = status;
			emailStr = email;
			dateStr = date;
			pincodeStr = pincode;
		}

		
		@SuppressWarnings("deprecation")
		@Override
		protected Boolean doInBackground(Void... params) {
			List <NameValuePair> nvps = new ArrayList <NameValuePair>();
			nvps.add(new BasicNameValuePair("status", statusStr));
			nvps.add(new BasicNameValuePair("email", emailStr));
			nvps.add(new BasicNameValuePair("pickupdate", dateStr));
			nvps.add(new BasicNameValuePair("pincode", pincodeStr));
			try {
				BasicCookieStore cookieStore = commonController.fetchCookie(nvps,UPDATE_WORKSTATUS_URL);
				List<Cookie> cookieList= cookieStore.getCookies();
				Cookie crnCookie = cookieList.get(1);
        					
        		} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;
		}
		


		@Override
		protected void onPostExecute(final Boolean success) {
			updateStatusTask = null;
			//stop the progress spinner
			dialog.hide();
		//mOverlayDialog.hide();  

			if (success) {
				//  login success and move to main Activity here.
				Toast.makeText(context,"Workstatus successfully changed to " + statusSpinner.getSelectedItem().toString(), Toast.LENGTH_LONG).show();
				Fragment fragment = null;
            	fragment = new HomeFragment();
            	FragmentManager fragmentManager = myContext.getSupportFragmentManager();
            	fragmentManager.beginTransaction()
            	.replace(R.id.content_frame, fragment)
            	.commit();
			} else {
				// login failure
				Toast.makeText(context,"Unable to change workstatus now. Please try after some time.	.", Toast.LENGTH_LONG).show();
			}

		}

		@Override
		protected void onCancelled() {
			updateStatusTask = null;
		}
	}
	
	public void showDatePicker() {
		// Initializiation
		LayoutInflater inflater = (LayoutInflater) getActivity().getLayoutInflater();
		final AlertDialog.Builder dialogBuilder = 
				new AlertDialog.Builder(context);
		View customView = inflater.inflate(R.layout.datepicker_layout, null);
		dialogBuilder.setView(customView);
		final Calendar now = Calendar.getInstance();
		final DatePicker datePicker = 
				(DatePicker) customView.findViewById(R.id.dialog_datepicker);
		final TextView dateTextView = 
				(TextView) customView.findViewById(R.id.dialog_dateview);
		final SimpleDateFormat dateViewFormatter = 
				new SimpleDateFormat("EEEE, dd.MM.yyyy", Locale.ENGLISH);
		final SimpleDateFormat formatter = 
				new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
		// Minimum date
		Calendar minDate = Calendar.getInstance();
		try {
			minDate.setTime(formatter.parse("12.12.2010"));
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		datePicker.setMinDate(minDate.getTimeInMillis());
		// View settings
		dialogBuilder.setTitle("Choose a date");
		Calendar choosenDate = Calendar.getInstance();
		int year = choosenDate.get(Calendar.YEAR);
		int month = choosenDate.get(Calendar.MONTH);
		int day = choosenDate.get(Calendar.DAY_OF_MONTH);
		try {
			Date choosenDateFromUI = formatter.parse(
					statusDateTextView.getText().toString()
					);
			choosenDate.setTime(choosenDateFromUI);
			year = choosenDate.get(Calendar.YEAR);
			month = choosenDate.get(Calendar.MONTH);
			day = choosenDate.get(Calendar.DAY_OF_MONTH);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Calendar dateToDisplay = Calendar.getInstance();
		dateToDisplay.set(year, month, day);
		dateTextView.setText(
				dateViewFormatter.format(dateToDisplay.getTime())
				);
		// Buttons
		dialogBuilder.setPositiveButton(
				"Choose", 
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Calendar choosen = Calendar.getInstance();
						choosen.set(
								datePicker.getYear(), 
								datePicker.getMonth(), 
								datePicker.getDayOfMonth()
								);
/*						pickupDateTextView.setText(
								dateViewFormatter.format(choosen.getTime())
								);*/
						statusDateTextView.setText(String.valueOf(datePicker.getMonth()+1) + "-" + String.valueOf(datePicker.getDayOfMonth()) + "-" + String.valueOf(datePicker.getYear()));

						dialog.dismiss();
					}
				}
				);
		final AlertDialog dialog = dialogBuilder.create();
		// Initialize datepicker in dialog atepicker
		datePicker.init(
				year, 
				month, 
				day, 
				new DatePicker.OnDateChangedListener() {
					public void onDateChanged(DatePicker view, int year, 
							int monthOfYear, int dayOfMonth) {
						Calendar choosenDate = Calendar.getInstance();
						choosenDate.set(year, monthOfYear, dayOfMonth);
						dateTextView.setText(
								dateViewFormatter.format(choosenDate.getTime())
								);
						statusDateTextView.setText(String.valueOf(monthOfYear+1) + "-" + String.valueOf(dayOfMonth) + "-" + String.valueOf(year));

						/*
                    if (choosenDate.get(Calendar.DAY_OF_WEEK) == 
                        Calendar.SUNDAY || 
                        now.compareTo(choosenDate) < 0) {
                        dateTextView.setTextColor(
                            Color.parseColor("#ff0000")
                        );
                        ((Button) dialog.getButton(
                        AlertDialog.BUTTON_POSITIVE))
                            .setEnabled(false);
                    } else {*/
						dateTextView.setTextColor(
								Color.parseColor("#000000")
								);
						((Button) dialog.getButton(
								AlertDialog.BUTTON_POSITIVE))
								.setEnabled(true);
						//}
					}
				}
				);
		// Finish
		dialog.show();
	}
}