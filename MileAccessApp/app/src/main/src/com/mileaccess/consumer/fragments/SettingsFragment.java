package com.mileaccess.consumer.fragments;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.android.mileaccess.R;
import com.mileaccess.consumer.HomeActivity;
import com.mileaccess.consumer.LoginActivity;
import com.mileaccess.consumer.HomeActivity.UserLogoutTask;
import com.mileaccess.consumer.controllers.CommonController;
import com.mileaccess.consumer.utilities.PreferencesManager;
import com.mileaccess.consumer.utilities.PreferencesManagerConstants;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SettingsFragment extends BaseFragment{

	AutoCompleteTextView orderNumber;
	Button btnTrack;
	private FragmentActivity myContext;
	private UserDeleteTask userDeleteTask = null;
    ProgressDialog dialog;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		context = activity;
		myContext=(FragmentActivity) activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// Retrieving the currently selected item number
		// Creating view correspoding to the fragment
		View v = inflater.inflate(R.layout.fragment_settings, container, false);

		initializeScreen(v);
		return v;
	}

	@Override
	public void onPause()
	{
		super.onPause();
		if(dialog!=null)
		{
			dialog.dismiss();
		}
	}
	

	private void initializeScreen(View view) {
		LinearLayout changePasswordLayout = (LinearLayout) view.findViewById (R.id.changePassword);
		changePasswordLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Fragment fragment = null;
				fragment = new ResetPassword();
				FragmentManager fragmentManager = myContext.getSupportFragmentManager();
				fragmentManager.beginTransaction()
				.replace(R.id.content_frame, fragment)
				.commit();
			}
		});
		
		LinearLayout deleteAccountLayout = (LinearLayout) view.findViewById (R.id.deleteAccount);
		deleteAccountLayout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(context);  
		        //Uncomment the below code to Set the message and title from the strings.xml file  
		        //builder.setMessage(R.string.dialog_message) .setTitle(R.string.dialog_title);  
		          
		        //Setting message manually and performing action on button click  
		        builder.setMessage("Are you sure you want to Delete your MileAccess account?")  
		            .setCancelable(false)  
		            .setNegativeButton("No", new DialogInterface.OnClickListener() {  
		                public void onClick(DialogInterface dialog, int id) {  
		                }  
		            })  
		            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {  
		                public void onClick(DialogInterface dialog, int id) {  
		                //  Action for 'NO' Button 
		                performDeleteAccount();
		                dialog.cancel();  
		             }  
		            });  
		  
		        //Creating dialog box  
		        AlertDialog alert = builder.create();  
		        //Setting the title manually  
		        alert.setTitle("Terminate Service");  
		        alert.show();  
			}
		});
	}	
	
	
	public void performDeleteAccount(){
		if (userDeleteTask != null) {
			return;
		}
		userDeleteTask = new UserDeleteTask();
		userDeleteTask.execute((Void) null);
		Toast.makeText(myContext,"Account Deleted successfully.", Toast.LENGTH_LONG).show();
		Intent intent = new Intent(myContext,
				LoginActivity.class);
		if (intent != null) {
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
			myContext.startActivity(intent);
		}
		PreferencesManager.setBool(myContext, PreferencesManagerConstants.IS_USER_LOGGED_IN, false);
	
		//	showProgress(true);
	}
	
	
	public class UserDeleteTask extends AsyncTask<Void, Void, Boolean> 
	{
		UserDeleteTask() {
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			//this is where you should write your authentication code
			// or call external service
			// following try-catch just simulates network access
			try {
				Thread.sleep(10);
				try {
					doneDelete();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}             
			} catch (InterruptedException e) 
			{
				return false;
			}
			return true;

		}

		@Override
		protected void onPostExecute(final Boolean success) {
			userDeleteTask = null;
			//stop the progress spinner
			//showProgress(false);

			if (success) {
				//  login success and move to main Activity here.
			} else {
				// login failure				
				Toast.makeText(myContext,"Unable to delete.Please try after some time.", Toast.LENGTH_LONG).show();
			}
		}

		@Override
		protected void onCancelled() {
			userDeleteTask = null;
		}
	}


	public void doneDelete()
	{
		List <NameValuePair> nvps = new ArrayList <NameValuePair>();
		nvps.add(new BasicNameValuePair("delete_account","delete"));
		CommonController commonController = new CommonController(myContext);
		try {
			commonController.fetchCookie(nvps,"http://www.mileaccess.com/settings.html");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}