package com.mileaccess.consumer.fragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.message.BasicNameValuePair;

import com.android.mileaccess.R;
import com.android.mileaccess.R.layout;
import com.mileaccess.consumer.HomeActivity;
import com.mileaccess.consumer.LoginActivity;
import com.mileaccess.consumer.SigninAcitvity.UserSigninTask;
import com.mileaccess.consumer.controllers.CommonController;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class CreateOrderFragment extends BaseFragment{

	Spinner serviceTypeSpinner,recepientCitySpinner,deliveryTypeSpinner,pickupCitySpinner;
	AutoCompleteTextView nameTextView;
	EditText pickupDateTextView,phoneTextView,pincodeTextView,recepNameTextView,pincodeRecepTextView,phoneRecepTextView,deliveryAddressTextView,pickupAddressTextView;
	Button btnCreateOrder;
	CommonController commonController;
	OrderCreationTask orderCreationTask;
	String crn;
	final String CREATE_ORDER_URL = "http://mileaccess.com/create_order.html";
	private FragmentActivity myContext;
	Dialog mOverlayDialog;
    ProgressDialog dialog;

	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		context = activity;
		myContext=(FragmentActivity) activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		int position = getArguments().getInt("position");
		View v = inflater.inflate(R.layout.fragment_create_order, container, false);

		initializeScreen(v);
		return v;
	}


	private void initializeScreen(View view) {
		serviceTypeSpinner = (Spinner) view.findViewById(R.id.servicetypeSpinner);
		nameTextView = (AutoCompleteTextView) view.findViewById(R.id.name);
		pickupDateTextView = (EditText) view.findViewById(R.id.pickupdate);
		phoneTextView = (EditText) view.findViewById(R.id.phone);
		pickupAddressTextView = (EditText) view.findViewById(R.id.pickupaddress);        
		pincodeTextView = (EditText) view.findViewById(R.id.pincode);
		pickupCitySpinner = (Spinner) view.findViewById(R.id.pickupcitySpinner);
		recepNameTextView = (EditText) view.findViewById(R.id.recepname);
		phoneRecepTextView = (EditText) view.findViewById(R.id.phonerecep);
		deliveryAddressTextView = (EditText) view.findViewById(R.id.deliveryaddress);
		pincodeRecepTextView = (EditText) view.findViewById(R.id.pincoderecep);
		recepientCitySpinner = (Spinner) view.findViewById(R.id.recepcity);
		deliveryTypeSpinner = (Spinner) view.findViewById(R.id.deliverytypeSpinner);
		mOverlayDialog = new Dialog(context, android.R.style.Theme_Panel); //display an invisible overlay dialog to prevent user interaction and pressing back
		btnCreateOrder = (Button) view.findViewById(R.id.create_order_button);

		btnCreateOrder.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
/*				mOverlayDialog.setCancelable(false);
				mOverlayDialog.show(); */ 
				hideKeyboard(getActivity());
				initOrderCreation();
			}
		});

		pickupDateTextView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				//new DatePickerDialog1964(context).show();
				showDatePicker();
			}
		});

        commonController = new CommonController(context);
	}

	@Override
	public void onPause()
	{
		super.onPause();
		if(dialog!=null)
		{
			dialog.dismiss();
		}
	}

	public void showDatePicker() {
		// Initializiation
		LayoutInflater inflater = (LayoutInflater) getActivity().getLayoutInflater();
		final AlertDialog.Builder dialogBuilder = 
				new AlertDialog.Builder(context);
		View customView = inflater.inflate(R.layout.datepicker_layout, null);
		dialogBuilder.setView(customView);
		final Calendar now = Calendar.getInstance();
		final DatePicker datePicker = 
				(DatePicker) customView.findViewById(R.id.dialog_datepicker);
		final TextView dateTextView = 
				(TextView) customView.findViewById(R.id.dialog_dateview);
		final SimpleDateFormat dateViewFormatter = 
				new SimpleDateFormat("EEEE, dd.MM.yyyy", Locale.ENGLISH);
		final SimpleDateFormat formatter = 
				new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
		// Minimum date
		Calendar minDate = Calendar.getInstance();
		try {
			minDate.setTime(formatter.parse("12.12.2010"));
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		datePicker.setMinDate(minDate.getTimeInMillis());
		// View settings
		dialogBuilder.setTitle("Choose a date");
		Calendar choosenDate = Calendar.getInstance();
		int year = choosenDate.get(Calendar.YEAR);
		int month = choosenDate.get(Calendar.MONTH);
		int day = choosenDate.get(Calendar.DAY_OF_MONTH);
		try {
			Date choosenDateFromUI = formatter.parse(
					pickupDateTextView.getText().toString()
					);
			choosenDate.setTime(choosenDateFromUI);
			year = choosenDate.get(Calendar.YEAR);
			month = choosenDate.get(Calendar.MONTH);
			day = choosenDate.get(Calendar.DAY_OF_MONTH);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Calendar dateToDisplay = Calendar.getInstance();
		dateToDisplay.set(year, month, day);
		dateTextView.setText(
				dateViewFormatter.format(dateToDisplay.getTime())
				);
		// Buttons
		dialogBuilder.setPositiveButton(
				"Choose", 
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Calendar choosen = Calendar.getInstance();
						choosen.set(
								datePicker.getYear(), 
								datePicker.getMonth(), 
								datePicker.getDayOfMonth()
								);
/*						pickupDateTextView.setText(
								dateViewFormatter.format(choosen.getTime())
								);*/
						pickupDateTextView.setText(String.valueOf(datePicker.getMonth()+1) + "-" + String.valueOf(datePicker.getDayOfMonth()) + "-" + String.valueOf(datePicker.getYear()));

						dialog.dismiss();
					}
				}
				);
		final AlertDialog dialog = dialogBuilder.create();
		// Initialize datepicker in dialog atepicker
		datePicker.init(
				year, 
				month, 
				day, 
				new DatePicker.OnDateChangedListener() {
					public void onDateChanged(DatePicker view, int year, 
							int monthOfYear, int dayOfMonth) {
						Calendar choosenDate = Calendar.getInstance();
						choosenDate.set(year, monthOfYear, dayOfMonth);
						dateTextView.setText(
								dateViewFormatter.format(choosenDate.getTime())
								);
						pickupDateTextView.setText(String.valueOf(monthOfYear+1) + "-" + String.valueOf(dayOfMonth) + "-" + String.valueOf(year));

						/*
                    if (choosenDate.get(Calendar.DAY_OF_WEEK) == 
                        Calendar.SUNDAY || 
                        now.compareTo(choosenDate) < 0) {
                        dateTextView.setTextColor(
                            Color.parseColor("#ff0000")
                        );
                        ((Button) dialog.getButton(
                        AlertDialog.BUTTON_POSITIVE))
                            .setEnabled(false);
                    } else {*/
						dateTextView.setTextColor(
								Color.parseColor("#000000")
								);
						((Button) dialog.getButton(
								AlertDialog.BUTTON_POSITIVE))
								.setEnabled(true);
						//}
					}
				}
				);
		// Finish
		dialog.show();
	}

	public void initOrderCreation() {
		boolean cancelCreate = false;
		View focusView = null;
		
		//servicetype
		String servType = serviceTypeSpinner.getSelectedItem().toString();
		String name = nameTextView.getText().toString();
		String pickDate = pickupDateTextView.getText().toString();
		String phone = phoneTextView.getText().toString();
		String pickAddress = pickupAddressTextView.getText().toString();
		String pincode = pincodeTextView.getText().toString();
		//pickupCity
		String pickCity = pickupCitySpinner.getSelectedItem().toString();
		String recep = recepNameTextView.getText().toString();
		String phoneRecep = phoneRecepTextView.getText().toString();
		String recepAddress = deliveryAddressTextView.getText().toString();
		String recpPincode = pincodeRecepTextView.getText().toString();
		String recepCity = recepientCitySpinner.getSelectedItem().toString();
		//Spinner delivtype 
		String deliveType = deliveryTypeSpinner.getSelectedItem().toString();
		
		if (TextUtils.isEmpty(name)) {
			nameTextView.setError(getString(R.string.field_required));
			focusView = nameTextView;
			cancelCreate = true;
		}
		
		if (TextUtils.isEmpty(pickDate)) {
			pickupDateTextView.setError(getString(R.string.field_required));
			focusView = pickupDateTextView;
			cancelCreate = true;
		}
		if (TextUtils.isEmpty(phone)) {
			phoneTextView.setError(getString(R.string.field_required));
			focusView = phoneTextView;
			cancelCreate = true;
		}
		if (TextUtils.isEmpty(pickAddress)) {
			pickupAddressTextView.setError(getString(R.string.field_required));
			focusView = pickupAddressTextView;
			cancelCreate = true;
		}
		if (TextUtils.isEmpty(pincode)) {
			pincodeTextView.setError(getString(R.string.field_required));
			focusView = pincodeTextView;
			cancelCreate = true;
		}
		if (TextUtils.isEmpty(recep)) {
			recepNameTextView.setError(getString(R.string.field_required));
			focusView = recepNameTextView;
			cancelCreate = true;
		}
		if (TextUtils.isEmpty(phoneRecep)) {
			phoneRecepTextView.setError(getString(R.string.field_required));
			focusView = phoneRecepTextView;
			cancelCreate = true;
		}
		if (TextUtils.isEmpty(recepAddress)) {
			deliveryAddressTextView.setError(getString(R.string.field_required));
			focusView = deliveryAddressTextView;
			cancelCreate = true;
		}
		if (TextUtils.isEmpty(recpPincode)) {
			pincodeRecepTextView.setError(getString(R.string.field_required));
			focusView = pincodeRecepTextView;
			cancelCreate = true;
		}
		
		if (cancelCreate) {
			// error in login
			focusView.requestFocus();
		} else {
		    dialog = ProgressDialog.show(context, "", "Please wait...", true);
			orderCreationTask = new OrderCreationTask(servType,name, pickDate,phone,pickAddress,pincode,pickCity,recep,phoneRecep,recepAddress,recpPincode,recepCity,deliveType);
			orderCreationTask.execute((Void) null);
		}
	}
	
	
	
	
	public class OrderCreationTask extends AsyncTask<Void, Void, Boolean> 
	{

		private final String serviceTypeStr;
		private final String nameStr;
		private final String pickupDateStr;
		private final String phoneStr;
		private final String pickupAddressStr;
		private final String pincodeStr;
		private final String pickCityStr;
		private final String recepNameStr;
		private final String recepPhoneStr;
		private final String recepAddressStr;
		private final String recepPincodeStr;
		private final String recepCityStr;
		private final String delevieryTypeStr;


		OrderCreationTask(String servType,String name, String pickDate, String phone,String pickAddress, String pincode, String pickCity,String recep,String phoneRecep, String recepAddress, String recpPincode, String recepCity,String deliveType) {
			serviceTypeStr = servType;
			nameStr = name;
			pickupDateStr = pickDate;
			phoneStr = phone;
			pickupAddressStr = pickAddress;
			pincodeStr = pincode;
			pickCityStr = pickCity;
			recepNameStr = recep;
			recepPhoneStr = phoneRecep;
			recepAddressStr = recepAddress;
			recepPincodeStr = recpPincode;
			recepCityStr = recepCity;
			delevieryTypeStr = deliveType;
		}

		
		@Override
		protected Boolean doInBackground(Void... params) {
			List <NameValuePair> nvps = new ArrayList <NameValuePair>();
			nvps.add(new BasicNameValuePair("servicetype", serviceTypeStr));
			nvps.add(new BasicNameValuePair("name", nameStr));
			nvps.add(new BasicNameValuePair("pickupdate", pickupDateStr));
			nvps.add(new BasicNameValuePair("contact", phoneStr));
			nvps.add(new BasicNameValuePair("pickupaddress", pickupAddressStr));
			nvps.add(new BasicNameValuePair("source_pincode", pincodeStr));
			nvps.add(new BasicNameValuePair("sourcecity", pickCityStr));
			nvps.add(new BasicNameValuePair("recipient_name", recepNameStr));
			nvps.add(new BasicNameValuePair("recipient_contact", recepPhoneStr));
			nvps.add(new BasicNameValuePair("deliveryaddress", recepAddressStr));
			nvps.add(new BasicNameValuePair("destination_pincode", recepPincodeStr));
			nvps.add(new BasicNameValuePair("destinationcity", recepCityStr));
			nvps.add(new BasicNameValuePair("Delivery", delevieryTypeStr));
			try {
				BasicCookieStore cookieStore = commonController.fetchCookie(nvps,CREATE_ORDER_URL);
				List<Cookie> cookieList= cookieStore.getCookies();
				Cookie crnCookie = cookieList.get(1);
        		if((crnCookie.getName()).equals("crn"))
        		{
        			crn = crnCookie.getValue();
        		}			
        		} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;
		}
		


		@Override
		protected void onPostExecute(final Boolean success) {
		orderCreationTask = null;
			//stop the progress spinner
		dialog.hide();
		//mOverlayDialog.hide();  

			if (success) {
				//  login success and move to main Activity here.
				Toast.makeText(context,"Order Succesfully Created. " + crn, Toast.LENGTH_LONG).show();
				AlertDialog.Builder builder = new AlertDialog.Builder(context);  
		        //Uncomment the below code to Set the message and title from the strings.xml file  
		        //builder.setMessage(R.string.dialog_message) .setTitle(R.string.dialog_title);  
		          
		        //Setting message manually and performing action on button click  
		        builder.setMessage("Your Order CRN is " + crn)  
		            .setCancelable(false)  
		            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {  
		                public void onClick(DialogInterface dialog, int id) {  
		                	Fragment fragment = null;
		                	fragment = new HomeFragment();
		                	FragmentManager fragmentManager = myContext.getSupportFragmentManager();
		                	fragmentManager.beginTransaction()
		                	.replace(R.id.content_frame, fragment)
		                	.commit();
		                	dialog.cancel();  
		             }  
		            });  
		  
		        //Creating dialog box  
		        AlertDialog alert = builder.create();  
		        //Setting the title manually  
		        alert.setTitle("Order created successfully");  
		        alert.show();  
			} else {
				// login failure
				Toast.makeText(context,"Incorrect Details. Unable to create order.", Toast.LENGTH_LONG).show();
			}

		}

		@Override
		protected void onCancelled() {
			orderCreationTask = null;
		}
	}
}