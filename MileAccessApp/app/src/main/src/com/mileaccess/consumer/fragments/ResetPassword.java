package com.mileaccess.consumer.fragments;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.message.BasicNameValuePair;

import com.android.mileaccess.R;
import com.mileaccess.consumer.HomeActivity;
import com.mileaccess.consumer.LoginActivity;
import com.mileaccess.consumer.controllers.CommonController;
import com.mileaccess.consumer.controllers.LoginController;
import com.mileaccess.consumer.fragments.CreateOrderFragment.OrderCreationTask;
import com.mileaccess.consumer.utilities.PreferencesManager;
import com.mileaccess.consumer.utilities.PreferencesManagerConstants;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ResetPassword extends BaseFragment{
	
	AutoCompleteTextView emailTextView;
	EditText oldPasswordTextView, confirmoldPasswordTextView, newpasswordTextView;
	Button updateButton;
	CommonController commonController;
	String RESET_PASSWORD_URL= "http://www.mileaccess.com/settings.html";
	ResetPasswordTask resetPasswordTask;
    ProgressDialog dialog;	
    HomeActivity mainActivity;   

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		context = getActivity();
		mainActivity = (HomeActivity) activity;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_change_password, container, false);
		initializeScreen(v);
		return v;
	}
	
	@Override
	public void onPause()
	{
		super.onPause();
		if(dialog!=null)
		{
			dialog.dismiss();
		}
	}
	
	private void initializeScreen(View view) {
		emailTextView = (AutoCompleteTextView) view.findViewById(R.id.email);
		oldPasswordTextView = (EditText) view.findViewById(R.id.oldpassword);
		confirmoldPasswordTextView = (EditText) view.findViewById(R.id.confirmoldpassword);
		newpasswordTextView = (EditText) view.findViewById(R.id.newpassword); 
		updateButton = (Button) view.findViewById(R.id.update_button);
		updateButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				hideKeyboard(getActivity());
				initResetPassword();
			}
		});
        emailTextView.setText(PreferencesManager.getString(context, PreferencesManagerConstants.USER_EMAIL, ""));
		commonController = new CommonController(context);
	}
	
	public void initResetPassword()
	{
		String email = emailTextView.getText().toString();
		String oldPassword = oldPasswordTextView.getText().toString();
		String confirmOldPassword = confirmoldPasswordTextView.getText().toString();
		String newPassword = newpasswordTextView.getText().toString();
		
		boolean cancelReset = false;
		View focusView = null;
		
		if (TextUtils.isEmpty(email)) {
			emailTextView.setError(getString(R.string.field_required));
			focusView = emailTextView;
			cancelReset = true;
		}
		if (TextUtils.isEmpty(oldPassword)) {
			oldPasswordTextView.setError(getString(R.string.field_required));
			focusView = oldPasswordTextView;
			cancelReset = true;
		}
		if (TextUtils.isEmpty(confirmOldPassword)) {
			confirmoldPasswordTextView.setError(getString(R.string.field_required));
			focusView = confirmoldPasswordTextView;
			cancelReset = true;
		}
		if (TextUtils.isEmpty(newPassword)) {
			newpasswordTextView.setError(getString(R.string.field_required));
			focusView = newpasswordTextView;
			cancelReset = true;
		}
		
		if (cancelReset) {
			// error in login
			focusView.requestFocus();
		} else {
			dialog = ProgressDialog.show(context, "", "Please wait...", true);
			if(oldPassword.equals(confirmOldPassword))
			{
				resetPasswordTask = new ResetPasswordTask(email,oldPassword,newPassword);
				resetPasswordTask.execute((Void) null);
			}
			else
			{
				Toast.makeText(context,"Old passwords doesnt match.Please enter correct password.", Toast.LENGTH_LONG).show();
			}
		}
	}
	
	public class ResetPasswordTask extends AsyncTask<Void, Void, Boolean> 
	{
//pwd1 = newpass    pwd2  = oldpass
		private final String emailStr;
		private final String oldPassStr;
        private final String newPassStr;
		
		ResetPasswordTask(String email, String oldPass, String newPass) {
			emailStr = email;
			oldPassStr = oldPass;
			newPassStr = newPass;
		}

		@SuppressWarnings("deprecation")
		@Override
		protected Boolean doInBackground(Void... params) {

			boolean flag = true;

			List <NameValuePair> nvps = new ArrayList <NameValuePair>();
			nvps.add(new BasicNameValuePair("email", emailStr));
			nvps.add(new BasicNameValuePair("pwd1", newPassStr));
			nvps.add(new BasicNameValuePair("pwd2", oldPassStr));

			try {
				@SuppressWarnings("unused")
				BasicCookieStore cookieStore = commonController.fetchCookie(nvps,RESET_PASSWORD_URL);
			
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return flag;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			resetPasswordTask = null;
			dialog.hide();
			//stop the progress spinner
			if (success) {
				mainActivity.performLogout();
			} else {
				Toast.makeText(context,"Unable to change password.Please try after sometime.", Toast.LENGTH_LONG).show();
			}
		}

		@Override
		protected void onCancelled() {
			resetPasswordTask = null;
		}
	}
}