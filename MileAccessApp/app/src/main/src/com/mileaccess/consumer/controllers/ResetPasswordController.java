package com.mileaccess.consumer.controllers;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;

import android.content.Context;

public class ResetPasswordController {
	protected Context context;
    public static List<Cookie> cookies;
    public static Cookie cookie1;
    public static Date date;
    
    
    public ResetPasswordController(Context context){
        this.context = context;
    }

    @SuppressWarnings("deprecation")
	public List<Cookie> fetchResetCookie(List <NameValuePair> nvps) throws IOException
	{

		DefaultHttpClient httpclient = new DefaultHttpClient();

		HttpPost httpost = new HttpPost("http://www.mileaccess.com/password_reset.html");



		httpost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));

		HttpResponse response = httpclient.execute(httpost);
		HttpEntity entity = response.getEntity();

		System.out.println("Login form get: " + response.getStatusLine());
		if (entity != null) {
			entity.consumeContent();
		}

		System.out.println("Post logon cookies:");
		cookies = httpclient.getCookieStore().getCookies();
		if (cookies.isEmpty()) {
			System.out.println("None");
		} else {
			cookie1 = cookies.get(0);
			for (int i = 0; i < cookies.size(); i++) {
				System.out.println("- " + cookies.get(i).toString());
				// date = cookies.get(i).toString().substring(cookies.get(i).toString().indexOf("expiry: "), cookies.get(i).toString().indexOf("],"));                 
			}
			//date = cookies.get(0).getExpiryDate();
		}
		httpclient.getConnectionManager().shutdown();    
		return cookies;
	}

}
