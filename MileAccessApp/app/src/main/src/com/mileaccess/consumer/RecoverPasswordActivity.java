package com.mileaccess.consumer;



import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.cookie.Cookie;
import org.apache.http.message.BasicNameValuePair;

import com.android.mileaccess.R;
import com.mileaccess.consumer.controllers.ResetPasswordController;
import com.mileaccess.consumer.fragments.HomeFragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

public class RecoverPasswordActivity extends Activity {

	private AutoCompleteTextView emailTextView;
	ResetPasswordController resetPassword;
	private ResetPasswordTask resetPasswordTask = null;
	private View signinFormView;
	private View progressView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
				WindowManager.LayoutParams.FLAG_FULLSCREEN);  
		setContentView(R.layout.activity_forgotpassword);
		resetPassword = new ResetPasswordController(RecoverPasswordActivity.this);
		emailTextView = (AutoCompleteTextView) findViewById(R.id.email);

		signinFormView = findViewById(R.id.login_form);
		progressView = findViewById(R.id.login_progress);

		Button signButton = (Button) findViewById(R.id.email_sign_in_button);
		signButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				initReset();
			}
		});
	}

	public void initReset() {
		if (resetPasswordTask != null) {
			return;
		}

		emailTextView.setError(null);

		String email = emailTextView.getText().toString();

		boolean cancelLogin = false;
		View focusView = null;

		if (TextUtils.isEmpty(email)) {
			emailTextView.setError(getString(R.string.field_required));
			focusView = emailTextView;
			cancelLogin = true;
		} 

		if (cancelLogin) {
			// error in login
			focusView.requestFocus();
		} else {
			// show progress spinner, and start background task to login
			showProgress(true);
			resetPasswordTask = new ResetPasswordTask(email);
			resetPasswordTask.execute((Void) null);
		}
	}



	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	public void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

			signinFormView.setVisibility(show ? View.GONE : View.VISIBLE);
			signinFormView.animate().setDuration(shortAnimTime).alpha(
					show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							signinFormView.setVisibility(show ? View.GONE : View.VISIBLE);
						}
					});

			progressView.setVisibility(show ? View.VISIBLE : View.GONE);
			progressView.animate().setDuration(shortAnimTime).alpha(
					show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							progressView.setVisibility(show ? View.VISIBLE : View.GONE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			progressView.setVisibility(show ? View.VISIBLE : View.GONE);
			signinFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}



	public class ResetPasswordTask extends AsyncTask<Void, Void, Boolean> 
	{
		private final String emailStr;

		ResetPasswordTask(String email) {
			emailStr = email;
		}

		@SuppressWarnings("deprecation")
		@Override
		protected Boolean doInBackground(Void... params) {
    	    boolean flag = false;
			try {
				Thread.sleep(1000);
				List <NameValuePair> nvps = new ArrayList <NameValuePair>();
				nvps.add(new BasicNameValuePair("email", emailStr));
				try {
					List<Cookie> cookies = resetPassword.fetchResetCookie(nvps);
					if(cookies.isEmpty() || cookies == null)
					{
    	        		flag = false;
					}
					else
					{
						if((cookies.get(1).getName()).equals("status"))
    	        		{
							if((cookies.get(1).getValue()).equals("success"))
							{
								flag = true;
							}
							else
							{
								flag = false;
							}
    	        		}
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					flag = false;
					e.printStackTrace();
				}             
			} catch (InterruptedException e) 
			{
				return false;
			}
			return flag;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			resetPasswordTask = null;
			//stop the progress spinner
			showProgress(false);

			if (success) {
				AlertDialog.Builder builder = new AlertDialog.Builder(RecoverPasswordActivity.this);  
				//Uncomment the below code to Set the message and title from the strings.xml file  
				//builder.setMessage(R.string.dialog_message) .setTitle(R.string.dialog_title);  

				//Setting message manually and performing action on button click  
				builder.setMessage("We have sent a password reset link to your email.")  
				.setCancelable(false)  
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {  
					public void onClick(DialogInterface dialog, int id) {  
						Intent intent = new Intent(getApplicationContext(),
								LoginActivity.class);
						if (intent != null) {
							intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
							getApplicationContext().startActivity(intent);
						}
						dialog.cancel();  
						finish();
					}  
				});  

				//Creating dialog box  
				AlertDialog alert = builder.create();  
				//Setting the title manually  
				alert.setTitle("Reset Password.");  
				alert.show();  
			} else {
				// login failure
				Toast.makeText(getApplicationContext(),"We are unable to reset password at this moment..", Toast.LENGTH_LONG);
			}
		}

		@Override
		protected void onCancelled() {
			resetPasswordTask = null;
			showProgress(false);
		}
	}

}
