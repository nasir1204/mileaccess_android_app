package com.mileaccess.consumer.fragments;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.message.BasicNameValuePair;

import com.android.mileaccess.R;
import com.mileaccess.consumer.HomeActivity;
import com.mileaccess.consumer.controllers.HomeController;
import com.mileaccess.consumer.utilities.ExpandableListAdapter;
import com.mileaccess.consumer.utilities.PreferencesManager;
import com.mileaccess.consumer.utilities.PreferencesManagerConstants;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.TableLayout.LayoutParams;

public class HomeFragment extends BaseFragment{

	AutoCompleteTextView orderNumber;
	Button btnTrack;
	private OrderDetailsTask orderDetailsTask;
	private OrderListTask orderListTask;
	private HomeController homeController;
	private Cookie orderListCookie;
	ExpandableListAdapter listAdapter;
	ExpandableListView expListView;
	List<String> listDataHeader;
	List<String> listStatusHeader;
	HashMap<String, List<String>> listDataChild;
	List<String> crnList = null;
	//String[] crnList;
	//String[] statusList;
	List<String> statusList = null;
	List<String> orderDetailsList;
	List<Cookie> cookieList;
	HomeActivity mainActivity;   
	private ProgressBar spinner;
    TextView noOrders;
    List<String> edittedOrderDetailsList;
    //ProgressDialog dialog;


	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		context = activity;
		mainActivity = (HomeActivity) activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		View v = inflater.inflate(R.layout.fragment_home, container, false);

		// Getting reference to the TextView of the Fragment
		//TextView tv = (TextView) v.findViewById(R.id.tv_content);

		// Setting currently selected river name in the TextView
		//tv.setText(countries[position]);
		//new DatePickerDialog1964(context).show();

		initializeScreen(v);
		return v;
	}
	
	@Override
	public void onPause()
	{
		super.onPause();
/*		if(dialog!=null)
		{
			dialog.dismiss();
		}*/
	}
	
	
	@Override
	public void onDestroy()
	{
		super.onDestroy();
		mainActivity.dbHandler.deleteAllContact();
	}


	private void initializeScreen(View view) {
		//tv=(TableLayout) view.findViewById(R.id.table);
		  homeController = new HomeController(context);
	      spinner=(ProgressBar)view.findViewById(R.id.progressBar);
	      spinner.setVisibility(View.GONE);

	      noOrders = (TextView) view.findViewById(R.id.textNoOrder);
	      noOrders.setVisibility(View.GONE);

		if(PreferencesManager.getBool(context, PreferencesManagerConstants.IS_USER_LOGGED_IN, false))
		{
			orderListTask = new OrderListTask();
			orderListTask.execute((Void) null);
		    spinner.setVisibility(View.VISIBLE);
		      //dialog = ProgressDialog.show(context, "", "Please wait...", true);

		}



		// get the listview
		expListView = (ExpandableListView) view.findViewById(R.id.lvExp);

		listDataHeader = new ArrayList<String>();
		listStatusHeader = new ArrayList<String>();
		listDataChild = new HashMap<String, List<String>>();


		expListView.setOnGroupClickListener(new OnGroupClickListener() {

			@Override
			public boolean onGroupClick(ExpandableListView parent, View v,
					int groupPosition, long id) {
//				 Toast.makeText(context,
//				 "Group Clicked " + listDataHeader.get(groupPosition),
//				 Toast.LENGTH_SHORT).show();
				String crn = listDataHeader.get(groupPosition);
				if(mainActivity.dbHandler.getOrderDetails(crn) == null)
				{
					orderDetailsTask = new OrderDetailsTask(groupPosition,crn);
					orderDetailsTask.execute((Void) null);	
				}
		        return false;
			}
		});

		// Listview Group expanded listener
		expListView.setOnGroupExpandListener(new OnGroupExpandListener() {

			@Override
			public void onGroupExpand(int groupPosition) {
//				Toast.makeText(context,
//						listDataHeader.get(groupPosition) + " Expanded",
//						Toast.LENGTH_SHORT).show();

			}
		});

		// Listview Group collasped listener
		expListView.setOnGroupCollapseListener(new OnGroupCollapseListener() {

			@Override
			public void onGroupCollapse(int groupPosition) {
				Toast.makeText(context,
						listDataHeader.get(groupPosition) + " Collapsed",
						Toast.LENGTH_SHORT).show();

			}
		});

		// Listview on child click listener
		expListView.setOnChildClickListener(new OnChildClickListener() {

			@Override
			public boolean onChildClick(ExpandableListView parent, View v,
					int groupPosition, int childPosition, long id) {
				// TODO Auto-generated method stub
				Toast.makeText(
						context,
						listDataHeader.get(groupPosition)
						+ " : "
						+ listDataChild.get(
								listDataHeader.get(groupPosition)).get(
										childPosition), Toast.LENGTH_SHORT)
										.show();
				return false;
			}
		});
	}	


	
	public class OrderDetailsTask extends AsyncTask<Void, Void, Boolean> 
	{

		String crn;
		int position;
		List<Cookie> orderDetails = null;
		OrderDetailsTask(int groupPostion,String crnValue) {
			position = groupPostion;
			crn = crnValue;
		}

		@SuppressWarnings("deprecation")
		@Override
		protected Boolean doInBackground(Void... params) {
			//tv.removeAllViewsInLayout();
			try {
				//stuff that updates ui
				//http://stackoverflow.com/questions/4968226/show-data-in-table-view-in-android
				//homeController.fetchPreviousOrderList();
				BasicCookieStore cookieStore = null;
				try {
					List <NameValuePair> nvps = new ArrayList <NameValuePair>();
	    	        nvps.add(new BasicNameValuePair("order", crn));
					cookieStore = homeController.fetchDetailedOrder(nvps);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				cookieList = cookieStore.getCookies();
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						int i = 0;
						for(i = 0; i < cookieList.size();i++)
						{
							orderListCookie = cookieList.get(i);
							if((orderListCookie.getName()).equals("order_details"))
							{
								String orderDetails = orderListCookie.getValue();
								orderDetails = orderDetails.replace("+", " ");
								mainActivity.dbHandler.addOrderDetails(crn, orderDetails);
								orderDetailsList = new ArrayList<String>(Arrays.asList(orderDetails.split("\\|")));
								edittedOrderDetailsList  = new ArrayList<String>();
								
								for(int k = 0; k < orderDetailsList.size();k++)
								{
									if(k==0)
									{
										edittedOrderDetailsList.add(k, "CRN Number : "+orderDetailsList.get(k));
									}
									else if(k==1)
									{
										edittedOrderDetailsList.add(k, "Sender`s Name : "+orderDetailsList.get(k));
									}
									else if(k==2)
									{
										edittedOrderDetailsList.add(k, "Senders Contact : "+orderDetailsList.get(k));
									}
									else if(k==3)
									{
										edittedOrderDetailsList.add(k, "Pickup Address : "+orderDetailsList.get(k));
									}
									else if(k==4)
									{
										edittedOrderDetailsList.add(k, "Receiver`s Name : "+orderDetailsList.get(k));
									}
									else if(k==5)
									{
										edittedOrderDetailsList.add(k, "Contact Number : "+orderDetailsList.get(k));
									}
									else if(k==6)
									{
										edittedOrderDetailsList.add(k, "Delivery Address : "+orderDetailsList.get(k));
									}
									else if(k==7)
									{
										edittedOrderDetailsList.add(k, "Pickup Date : "+orderDetailsList.get(k));
									}
									else if(k==8)
									{
										edittedOrderDetailsList.add(k, "Delivery Type : "+orderDetailsList.get(k));
									}
									else if(k==9)
									{
										edittedOrderDetailsList.add(k, "Service Type : "+orderDetailsList.get(k));
									}
									else if(k==10)
									{
										edittedOrderDetailsList.add(k, "Status : "+orderDetailsList.get(k));
									}
								}
							}
						}		

					    listDataChild.put(listDataHeader.get(position), edittedOrderDetailsList);
						
						listAdapter = new ExpandableListAdapter(context,crnList,statusList,listDataChild);
						expListView.setAdapter(listAdapter);
				    	expListView.expandGroup(position);
					}
				});
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;

		}

		@Override
		protected void onPostExecute(final Boolean success) {
			orderDetailsTask = null;
			//stop the progress spinner
			if (success) {
				//  login success and move to main Activity here.
			} else {
				// login failure
				Toast.makeText(context,"Incorrect Details. Unable to create profile.", Toast.LENGTH_LONG).show();
			}

		}
		@Override
		protected void onCancelled() {
			orderDetailsTask = null;

		}
	}

	public class OrderListTask extends AsyncTask<Void, Void, Boolean> 
	{
		List<Cookie> orderDetails = null;
		OrderListTask() {

		}

		@SuppressWarnings("deprecation")
		@Override
		protected Boolean doInBackground(Void... params) {
			//tv.removeAllViewsInLayout();
			try {
				//stuff that updates ui
				//http://stackoverflow.com/questions/4968226/show-data-in-table-view-in-android
				//homeController.fetchPreviousOrderList();
				BasicCookieStore cookieStore = null;
				try {
					cookieStore = homeController.fetchPreviousOrderList();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				finally
				{
				      
				}
				cookieList = cookieStore.getCookies();
				getActivity().runOnUiThread(new Runnable() {
					@Override
					public void run() {
						spinner.setVisibility(View.GONE);
						//dialog.hide();
						for(int i = 0; i < cookieList.size();i++)
						{
							orderListCookie = cookieList.get(i);
							if((orderListCookie.getName()).equals("order_ids"))
							{
								String crns = orderListCookie.getValue();
								if(!TextUtils.isEmpty(crns))
								{
									crnList = new ArrayList<String>(Arrays.asList(crns.split("\\|")));
									//crnList = crns.split("\\|");
									int a = crnList.size();
									for (int j = 0; j < crnList.size(); j++) {
										System.out.println(crnList.get(j));
										listDataHeader.add(crnList.get(j));
									}
								}
								else
								{
									Toast.makeText(context,"No orders", Toast.LENGTH_LONG).show();

								      noOrders.setVisibility(View.VISIBLE);
								}
							}
							if((orderListCookie.getName()).equals("order_status"))
							{
								String status = orderListCookie.getValue();
								if(!TextUtils.isEmpty(status))
								{
									status = status.replace("+", " ");
									statusList = new ArrayList<String>(Arrays.asList(status.split("\\|")));
									//statusList = status.split("|");
									int a = crnList.size();
									for (int j = 0; j < statusList.size(); j++) {
										System.out.println(statusList.get(j));
										listDataHeader.add(statusList.get(j));
									}
								}
							}
						}		

						List<String> top250 = new ArrayList<String>();
						top250.add("  Fetching Details ... Please wait. ");
						if(crnList!=null)
						{
						int k = 0;	
						for(k = 0; k < crnList.size();k++)
						{
							listDataChild.put(listDataHeader.get(k), top250);
						}
						listAdapter = new ExpandableListAdapter(context,crnList,statusList,listDataChild);
						expListView.setAdapter(listAdapter);
						
						}
					}
				});
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;

		}

		@Override
		protected void onPostExecute(final Boolean success) {
			orderDetailsTask = null;
			//stop the progress spinner
			if (success) {
				//  login success and move to main Activity here.
			} else {
				// login failure
				Toast.makeText(context,"Incorrect Details. Unable to create profile.", Toast.LENGTH_LONG).show();
			}

		}
		@Override
		protected void onCancelled() {
			orderDetailsTask = null;

		}
	}

	/*
	private void prepareListData() {
		listDataHeader = new ArrayList<String>();
		listDataChild = new HashMap<String, List<String>>();

		// Adding child data
		listDataHeader.add("Top 250");
		listDataHeader.add("Now Showing");
		listDataHeader.add("Coming Soon..");

		// Adding child data
		List<String> top250 = new ArrayList<String>();
		top250.add("The Shawshank Redemption");
		top250.add("The Godfather");
		top250.add("The Godfather: Part II");
		top250.add("Pulp Fiction");
		top250.add("The Good, the Bad and the Ugly");
		top250.add("The Dark Knight");
		top250.add("12 Angry Men");

		List<String> nowShowing = new ArrayList<String>();
		nowShowing.add("The Conjuring");
		nowShowing.add("Despicable Me 2");
		nowShowing.add("Turbo");
		nowShowing.add("Grown Ups 2");
		nowShowing.add("Red 2");
		nowShowing.add("The Wolverine");

		List<String> comingSoon = new ArrayList<String>();
		comingSoon.add("2 Guns");
		comingSoon.add("The Smurfs 2");
		comingSoon.add("The Spectacular Now");
		comingSoon.add("The Canyons");
		comingSoon.add("Europa Report");

		listDataChild.put(listDataHeader.get(0), top250); // Header, Child data
		listDataChild.put(listDataHeader.get(1), nowShowing);
		listDataChild.put(listDataHeader.get(2), comingSoon);
	}
    */

}