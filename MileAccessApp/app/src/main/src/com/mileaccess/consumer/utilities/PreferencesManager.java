package com.mileaccess.consumer.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;



/**
 * The Class PreferencesManager.
 */
public class PreferencesManager {

	public static void setLong(Context context, String key, long value) {

		SharedPreferences.Editor editor = context
				.getSharedPreferences(
						PreferencesManagerConstants.PREF_NAME,
						Context.MODE_PRIVATE).edit();
		editor.putLong(key, value);
		editor.commit();

	}
	
	public static long getLong(Context context, String key, long defaultValue) {

		SharedPreferences prefs = context
				.getSharedPreferences(
						PreferencesManagerConstants.PREF_NAME,
						Context.MODE_PRIVATE);
		return prefs.getLong(key, defaultValue);
	}
	
	public static void setInt(Context context, String key, int value) {

		SharedPreferences.Editor editor = context
				.getSharedPreferences(
						PreferencesManagerConstants.PREF_NAME,
						Context.MODE_PRIVATE).edit();
		editor.putInt(key, value);

		editor.commit();

	}

	public static int getInt(Context context, String key, int defaultValue) {

		SharedPreferences prefs = context
				.getSharedPreferences(
						PreferencesManagerConstants.PREF_NAME,
						Context.MODE_PRIVATE);
		return prefs.getInt(key, defaultValue);
	}

	public static void setBool(Context context, String key, Boolean value) {

		SharedPreferences.Editor editor = context
				.getSharedPreferences(
						PreferencesManagerConstants.PREF_NAME,
						Context.MODE_PRIVATE).edit();

		editor.putBoolean(key, value);
		editor.commit();

	}

	public static Boolean getBool(Context context, String key,
			boolean defaultValue) {

		SharedPreferences prefs = context
				.getSharedPreferences(
						PreferencesManagerConstants.PREF_NAME,
						Context.MODE_PRIVATE);
		return prefs.getBoolean(key, defaultValue);

	}

	public static void setString(Context context, String key, String value) {

		SharedPreferences.Editor editor = context
				.getSharedPreferences(
						PreferencesManagerConstants.PREF_NAME,
						Context.MODE_PRIVATE).edit();

		editor.putString(key, value);
		editor.commit();

	}

	public static String getString(Context context, String key,
			String defaultValue) {

		SharedPreferences prefs = context
				.getSharedPreferences(
						PreferencesManagerConstants.PREF_NAME,
						Context.MODE_PRIVATE);
		return prefs.getString(key, defaultValue);

	}
	

	
	public static void clearDataAfterLogout(Context context) {
		setBool(context, PreferencesManagerConstants.IS_USER_LOGGED_IN, false);
		//setString(context, PreferencesManagerConstants.BOOKING_ID, null);
//		setString(context, PreferencesManagerConstants.BOOKING_TIME, null);
	}
}
