package com.mileaccess.consumer.fragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.android.mileaccess.R;
import com.android.mileaccess.R.layout;
import com.mileaccess.consumer.HomeActivity;
import com.mileaccess.consumer.LoginActivity;
import com.mileaccess.consumer.SigninAcitvity.UserSigninTask;
import com.mileaccess.consumer.controllers.CommonController;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class UpdateOrderFragment extends BaseFragment{

	Spinner statusSpinner,locationSpinner,deliveryTypeSpinner,paymentSpinner;
	AutoCompleteTextView pickerIdTextView;
	EditText crnTextView,deliverIdTextView,priceTextView,weightTextView,pincodeRecepTextView,lengthTextView,detailsTextView,discountTextView,distanceTextView,estimateTextView,transitTextView;
	Button btnUpdateOrder;
	CommonController commonController;
	UpdateOrderTask orderUpdateTask;
	String crn;
	final String UPDATE_ORDER_URL = "http://mileaccess.com/update_order.html";
	ProgressDialog dialog;
	private FragmentActivity myContext;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		context = getActivity();
		myContext=(FragmentActivity) activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		crn = getArguments().getString("crn");
		View v = inflater.inflate(R.layout.fragment_update_order, container, false);

		initializeScreen(v);
		return v;
	}


	private void initializeScreen(View view) {
		crnTextView = (EditText) view.findViewById(R.id.crn);
		statusSpinner = (Spinner) view.findViewById(R.id.statusSpinner);
		pickerIdTextView = (AutoCompleteTextView) view.findViewById(R.id.picker_id);
		deliverIdTextView = (AutoCompleteTextView) view.findViewById(R.id.deliver_id);
		priceTextView = (EditText) view.findViewById(R.id.price);
		discountTextView = (EditText) view.findViewById(R.id.discount);        
		paymentSpinner = (Spinner) view.findViewById(R.id.paymentSpinner);
		weightTextView = (EditText) view.findViewById(R.id.weight);
		lengthTextView = (EditText) view.findViewById(R.id.length);
		detailsTextView = (EditText) view.findViewById(R.id.details);
		locationSpinner = (Spinner) view.findViewById(R.id.location);
		distanceTextView = (EditText) view.findViewById(R.id.distance);
		estimateTextView = (EditText) view.findViewById(R.id.estimate);
		//transitTextView = (EditText) view.findViewById(R.id.transit);
		btnUpdateOrder = (Button) view.findViewById(R.id.update_order_button);

		btnUpdateOrder.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				hideKeyboard(getActivity());
				initOrderCreation();
			}
		});
        commonController = new CommonController(context);
        crnTextView.setText(crn);
	}

	@Override
	public void onPause()
	{
		super.onPause();
		if(dialog!=null)
		{
			dialog.dismiss();
		}
	}


	
	public void initOrderCreation() {
		if (orderUpdateTask != null) {
			return;
		}

		String status = statusSpinner.getSelectedItem().toString();
		String pickerId = pickerIdTextView.getText().toString();
		String deliverID = deliverIdTextView.getText().toString();
		String price = priceTextView.getText().toString();
		String discount = discountTextView.getText().toString();
		String paymentType = paymentSpinner.getSelectedItem().toString();
		String weight = weightTextView.getText().toString();
		String length = lengthTextView.getText().toString();
		String details = detailsTextView.getText().toString();
		String location = locationSpinner.getSelectedItem().toString();
		String distance = distanceTextView.getText().toString();
		String estimate = estimateTextView.getText().toString();
		//String transit = transitTextView.getText().toString();
		
		boolean cancelUpdate = false;
		View focusView = null;
		/*
		if (TextUtils.isEmpty(pickerId)) {
			pickerIdTextView.setError(getString(R.string.field_required));
			focusView = pickerIdTextView;
			cancelUpdate = true;
		}
		
		if (TextUtils.isEmpty(deliverID)) {
			deliverIdTextView.setError(getString(R.string.field_required));
			focusView = deliverIdTextView;
			cancelUpdate = true;
		}
		if (TextUtils.isEmpty(price)) {
			priceTextView.setError(getString(R.string.field_required));
			focusView = priceTextView;
			cancelUpdate = true;
		}
		if (TextUtils.isEmpty(discount)) {
			discountTextView.setError(getString(R.string.field_required));
			focusView = discountTextView;
			cancelUpdate = true;
		}
		if (TextUtils.isEmpty(weight)) {
			weightTextView.setError(getString(R.string.field_required));
			focusView = weightTextView;
			cancelUpdate = true;
		}
		if (TextUtils.isEmpty(length)) {
			lengthTextView.setError(getString(R.string.field_required));
			focusView = lengthTextView;
			cancelUpdate = true;
		}
		if (TextUtils.isEmpty(details)) {
			detailsTextView.setError(getString(R.string.field_required));
			focusView = detailsTextView;
			cancelUpdate = true;
		}
		if (TextUtils.isEmpty(distance)) {
			distanceTextView.setError(getString(R.string.field_required));
			focusView = distanceTextView;
			cancelUpdate = true;
		}
		if (TextUtils.isEmpty(estimate)) {
			estimateTextView.setError(getString(R.string.field_required));
			focusView = estimateTextView;
			cancelUpdate = true;
		}
		*/
		cancelUpdate = false;
		if (cancelUpdate) {
			// error in login
			//focusView.requestFocus();
		} else {
		    dialog = ProgressDialog.show(context, "", "Please wait...", true);
			orderUpdateTask = new UpdateOrderTask(status,pickerId, deliverID,price,discount,paymentType,weight,length,details,location,distance,estimate);
			orderUpdateTask.execute((Void) null);
		}
	}
	
	
	
	
	public class UpdateOrderTask extends AsyncTask<Void, Void, Boolean> 
	{

		private final String statusTypeStr;
		private final String pickerIdStr;
		private final String deliverIDStr;
		private final String priceStr;
		private final String discountStr;
		private final String paymentStr;
		private final String weightStr;
		private final String lengthStr;
		private final String detailsStr;
		private final String locationStr;
		private final String distanceStr;
		private final String estimateStr;
		//private final String transitStr;


		UpdateOrderTask(String status,String pickerId, String deliverID, String price,String discount, String payment, String weight,String length,String details, String location, String distance, String estimate) {
			statusTypeStr = status;
			pickerIdStr = pickerId;
			deliverIDStr = deliverID;
			priceStr = price;
			discountStr = discount;
			paymentStr = payment;
			weightStr = weight;
			lengthStr = length;
			detailsStr = details;
			locationStr = location;
			distanceStr = distance;
			estimateStr = estimate;
			//transitStr = transit;
		}
		
		@Override
		protected Boolean doInBackground(Void... params) {
			List <NameValuePair> nvps = new ArrayList <NameValuePair>();
			nvps.add(new BasicNameValuePair("order", crn));
			nvps.add(new BasicNameValuePair("status", statusTypeStr));
			nvps.add(new BasicNameValuePair("picker_id", pickerIdStr));
			nvps.add(new BasicNameValuePair("deliver_id", deliverIDStr));
			nvps.add(new BasicNameValuePair("price", priceStr));
			nvps.add(new BasicNameValuePair("discount", discountStr));
			nvps.add(new BasicNameValuePair("payment", paymentStr));
			nvps.add(new BasicNameValuePair("weight", weightStr));
			nvps.add(new BasicNameValuePair("length", lengthStr));
			nvps.add(new BasicNameValuePair("details", detailsStr));
			nvps.add(new BasicNameValuePair("location", locationStr));
			nvps.add(new BasicNameValuePair("distance", distanceStr));
			nvps.add(new BasicNameValuePair("estimate", estimateStr));
			//nvps.add(new BasicNameValuePair("transit", transitStr));
			try {
				commonController.fetchCookie(nvps,UPDATE_ORDER_URL);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;

		}

		@Override
		protected void onPostExecute(final Boolean success) {
		orderUpdateTask = null;
			//stop the progress spinner
		    dialog.hide();
			if (success) {
				//  login success and move to main Activity here.
				Toast.makeText(context,"Success Details.", Toast.LENGTH_LONG).show();
				Fragment fragment = null;
				fragment = new SelectCRNUpdateFragment();
				FragmentManager fragmentManager = myContext.getSupportFragmentManager();
				fragmentManager.beginTransaction()
				.replace(R.id.content_frame, fragment)
				.commit();
			} else {
				// login failure
				Toast.makeText(context,"Incorrect Details. Unable to create profile.", Toast.LENGTH_LONG).show();
			}

		}

		@Override
		protected void onCancelled() {
			orderUpdateTask = null;
		}
	}
}