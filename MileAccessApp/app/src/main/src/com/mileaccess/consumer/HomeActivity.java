package com.mileaccess.consumer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.message.BasicNameValuePair;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TableLayout;
import android.widget.TableLayout.LayoutParams;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.android.mileaccess.R;
import com.mileaccess.consumer.controllers.CommonController;
import com.mileaccess.consumer.controllers.HomeController;
import com.mileaccess.consumer.fragments.CreateOrderFragment;
import com.mileaccess.consumer.fragments.HomeFragment;
import com.mileaccess.consumer.fragments.SelectCRNUpdateFragment;
import com.mileaccess.consumer.fragments.SettingsFragment;
import com.mileaccess.consumer.fragments.TrackOrderFragment;
import com.mileaccess.consumer.fragments.UpdateOrderFragment;
import com.mileaccess.consumer.fragments.UpdateWorkstatusFragment;
import com.mileaccess.consumer.utilities.DatabaseHandler;
import com.mileaccess.consumer.utilities.PreferencesManager;
import com.mileaccess.consumer.utilities.PreferencesManagerConstants;

public class HomeActivity  extends ActionBarActivity {

	int mPosition = -1;	
	String mTitle = "";

	// Array of strings storing country names
	String[] mConsumer ;
	String[] mOperator ;

	// Array of integers points to images stored in /res/drawable-ldpi/
	int[] consumerIcons = new int[]{
			R.drawable.home,
			R.drawable.createorder,
			R.drawable.settings,
			R.drawable.logout,
	};
	
	int[] operatorIcons = new int[]{
			R.drawable.home,
			R.drawable.createorder,
			R.drawable.updatestatus,
			R.drawable.settings,
			R.drawable.logout,
	};

    public DatabaseHandler dbHandler;
	private DrawerLayout mDrawerLayout;	
	private ListView mDrawerList;	
	private ActionBarDrawerToggle mDrawerToggle;	
	private LinearLayout mDrawer ;	
	private List<HashMap<String,String>> mList ;	
	private SimpleAdapter mAdapter;	
	final private String COUNTRY = "country";	
	final private String FLAG = "flag";	
	final private String COUNT = "count";
	private String profileType;
	private UserLogoutTask userExitTask = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		dbHandler = new DatabaseHandler(this);
		profileType= getIntent().getStringExtra("profileType");

		Toast.makeText(this, 
				PreferencesManager.getString(this, PreferencesManagerConstants.PROFILE_TYPE, "consumer"), 
				Toast.LENGTH_SHORT).show();
		if(profileType==null)
		{
			profileType = PreferencesManager.getString(this, PreferencesManagerConstants.PROFILE_TYPE, "consumer");
		}
		//profileType = "Consumer";

		if(profileType.equals("Consumer"))
		{
			mConsumer = getResources().getStringArray(R.array.menuOptionsConsumer);
		}
		else if(profileType.equals("Operation"))
		{
			mConsumer = getResources().getStringArray(R.array.menuOptionsOperator);
		}

		// Title of the activity
		mTitle = (String)getTitle();

		mDrawerList = (ListView) findViewById(R.id.drawer_list);

		// Getting a reference to the sidebar drawer ( Title + ListView )
		mDrawer = ( LinearLayout) findViewById(R.id.drawer);

		// Each row in the list stores country name, count and flag
		mList = new ArrayList<HashMap<String,String>>();

		if(profileType.equals("Consumer"))
		{
			for(int i=0;i<mConsumer.length;i++){
				HashMap<String, String> hm = new HashMap<String,String>();
				hm.put(COUNTRY, mConsumer[i]);
				hm.put(FLAG, Integer.toString(consumerIcons[i]) );
				mList.add(hm);
			}
		}
		else if(profileType.equals("Operation"))
		{
			for(int i=0;i<mConsumer.length;i++){
				HashMap<String, String> hm = new HashMap<String,String>();
				hm.put(COUNTRY, mConsumer[i]);
				hm.put(FLAG, Integer.toString(operatorIcons[i]) );
				mList.add(hm);
			}
		}
		// Keys used in Hashmap 
		String[] from = { FLAG,COUNTRY,COUNT };

		// Ids of views in listview_layout
		int[] to = { R.id.flag , R.id.country , R.id.count};

		// Instantiating an adapter to store each items
		// R.layout.drawer_layout defines the layout of each item
		mAdapter = new SimpleAdapter(this, mList, R.layout.drawer_layout, from, to);

		// Getting reference to DrawerLayout
		mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);        

		// Creating a ToggleButton for NavigationDrawer with drawer event listener
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer , R.string.drawer_open,R.string.drawer_close){

			/** Called when drawer is closed */
			public void onDrawerClosed(View view) {               
				highlightSelectedCountry();            		
				supportInvalidateOptionsMenu();       
			}

			/** Called when a drawer is opened */
			public void onDrawerOpened(View drawerView) {            	
				getSupportActionBar().setTitle("Select a Category");            	
				supportInvalidateOptionsMenu();                
			}
		};

		// Setting event listener for the drawer
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		// ItemClick event handler for the drawer items
		mDrawerList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				mDrawerLayout.closeDrawer(mDrawer);	
				// Creating a Bundle object
				Bundle data = new Bundle();

				// Setting the index of the currently selected item of mDrawerList
				data.putInt("position", position);
				Fragment fragment = null;
				FragmentManager fragmentManager = getSupportFragmentManager(); // For AppCompat use getSupportFragmentManager
				switch(position) {
				default:
					break;
				case 0:
					fragment = new HomeFragment();
					break;
				case 1:
					if(profileType.equals("Consumer"))
					{
						fragment = new CreateOrderFragment();
					}
					else if(profileType.equals("Operation"))
					{
						fragment = new SelectCRNUpdateFragment();
					}
					break;
				case 2:	
					if(profileType.equals("Consumer"))
					{
						fragment = new SettingsFragment();
					}
					else if(profileType.equals("Operation"))
					{
						fragment = new UpdateWorkstatusFragment();
					}
					break;
				case 3:
					if(profileType.equals("Consumer"))
					{
						performLogout();
					}
					else if(profileType.equals("Operation"))
					{
						fragment = new SettingsFragment();
					}
					break;
				case 4:
					if(profileType.equals("Operation"))
					{
						performLogout();
					}
					break;
				}

				if(fragment!=null)
				{
					fragment.setArguments(data);
					fragmentManager.beginTransaction()
					.replace(R.id.content_frame, fragment)
					.commit();
				}

				//mDrawerList.setItemChecked(position, true);
				//mDrawerList.setSelection(position);
				mDrawerLayout.closeDrawer(mDrawer);

			}
		});


		// Enabling Up navigation
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);     

		getSupportActionBar().setDisplayShowHomeEnabled(true);  
		

		// Setting the adapter to the listView
		mDrawerList.setAdapter(mAdapter);   

		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(Color.rgb(40, 177, 230))); 
		launchHomeFragment();
	}

	public void launchHomeFragment()
	{
		Fragment fragment = null;
		fragment = new HomeFragment();
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager.beginTransaction()
		.replace(R.id.content_frame, fragment)
		.commit();
	}


	public void performLogout(){
		if (userExitTask != null) {
			return;
		}
		Toast.makeText(getApplicationContext(),"Logout successful.", Toast.LENGTH_LONG).show();
		Intent intent = new Intent(getApplicationContext(),
				LoginActivity.class);
		if (intent != null) {
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
			//intent.setFlags(Intent. FLAG_ACTIVITY_CLEAR_TOP);
			getApplicationContext().startActivity(intent);
		}
		finish();
		PreferencesManager.setBool(HomeActivity.this, PreferencesManagerConstants.IS_USER_LOGGED_IN, false);
	
		//	showProgress(true);
		userExitTask = new UserLogoutTask();
		userExitTask.execute((Void) null);
	}

	public class UserLogoutTask extends AsyncTask<Void, Void, Boolean> 
	{
		UserLogoutTask() {
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			//this is where you should write your authentication code
			// or call external service
			// following try-catch just simulates network access
			try {
				Thread.sleep(10);
				try {
					doneLogout();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}             
			} catch (InterruptedException e) 
			{
				return false;
			}
			return true;

		}

		@Override
		protected void onPostExecute(final Boolean success) {
			userExitTask = null;
			//stop the progress spinner
			//showProgress(false);

			if (success) {
				//  login success and move to main Activity here.
			} else {
				// login failure				
				Toast.makeText(getApplicationContext(),"Unable to logout.Please try after some time.", Toast.LENGTH_LONG).show();
			}


		}

		@Override
		protected void onCancelled() {
			userExitTask = null;
			//showProgress(false);
		}
	}


	public void doneLogout()
	{
		List <NameValuePair> nvps = new ArrayList <NameValuePair>();
		nvps.add(new BasicNameValuePair(" ", " "));
		CommonController commonController = new CommonController(this);
		try {
			commonController.fetchCookie(nvps,"http://www.mileaccess.com/logged_out.html");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void incrementHitCount(int position){
		HashMap<String, String> item = mList.get(position);
		String count = item.get(COUNT);
		item.remove(COUNT);
		if(count.equals("")){
			count = "  1  ";
		}else{
			int cnt = Integer.parseInt(count.trim());
			cnt ++;
			count = "  " + cnt + "  ";
		}				
		item.put(COUNT, count);				
		mAdapter.notifyDataSetChanged();
	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		//int id = item.getItemId();
		//if (id == R.id.action_settings) {
		//	return true;
		//	}

		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}



	// Highlight the selected country : 0 to 4
	public void highlightSelectedCountry(){
		int selectedItem = mDrawerList.getCheckedItemPosition();

		if(selectedItem > 2)
			mDrawerList.setItemChecked(mPosition, true);
		else
			mPosition = selectedItem;

		if(mPosition!=-1)
			getSupportActionBar().setTitle(mConsumer[mPosition]);
	}	
}
