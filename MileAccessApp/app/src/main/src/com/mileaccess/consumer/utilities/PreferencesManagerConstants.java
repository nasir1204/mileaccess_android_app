package com.mileaccess.consumer.utilities;

/**
 * The Class PreferencesManagerKey.
 */

public class PreferencesManagerConstants {
	public static final String PREF_NAME = "kpdriver";
	public static final String IS_USER_LOGGED_IN = "is_user_logged_in";
	public static final String USER_EMAIL = "user_email";
	public static final String PROFILE_TYPE = "profile_type";
	public static final String COOKIE_DATE = "cookie_date";
}