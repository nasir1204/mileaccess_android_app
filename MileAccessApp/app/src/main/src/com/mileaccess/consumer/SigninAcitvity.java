package com.mileaccess.consumer;



import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.android.mileaccess.R;
import com.mileaccess.consumer.controllers.SigninController;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SigninAcitvity extends Activity implements LoaderCallbacks<Cursor>{

	private UserSigninTask userSigninTask = null;
	private View signinFormView;
	private View progressView;
	private AutoCompleteTextView nameTextView;
	private AutoCompleteTextView emailTextView;
	private EditText passwordTextView;
	private EditText rePasswordTextView;
	private EditText phoneTextView;
    private SigninController signinController;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
                                        WindowManager.LayoutParams.FLAG_FULLSCREEN);  
		setContentView(R.layout.activity_signin);

		nameTextView = (AutoCompleteTextView) findViewById(R.id.name);
		emailTextView = (AutoCompleteTextView) findViewById(R.id.email);
        loadAutoComplete();
        
        passwordTextView = (EditText) findViewById(R.id.password);
        rePasswordTextView = (EditText) findViewById(R.id.repassword);
        phoneTextView = (EditText) findViewById(R.id.phone);
        
		signinFormView = findViewById(R.id.login_form);
		progressView = findViewById(R.id.login_progress);
		
		signinController = new SigninController(SigninAcitvity.this);
		
        Button signButton = (Button) findViewById(R.id.email_sign_in_button);
        signButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            	initSignin();
            }
        });
	}
	
    private void loadAutoComplete() {
        getLoaderManager().initLoader(0, null, this);
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.signin_acitvity, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	public void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

			signinFormView.setVisibility(show ? View.GONE : View.VISIBLE);
			signinFormView.animate().setDuration(shortAnimTime).alpha(
					show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							signinFormView.setVisibility(show ? View.GONE : View.VISIBLE);
						}
					});

			progressView.setVisibility(show ? View.VISIBLE : View.GONE);
			progressView.animate().setDuration(shortAnimTime).alpha(
					show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							progressView.setVisibility(show ? View.VISIBLE : View.GONE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			progressView.setVisibility(show ? View.VISIBLE : View.GONE);
			signinFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }
	
	
	public void initSignin() {
		if (userSigninTask != null) {
			return;
		}

		nameTextView.setError(null);
		emailTextView.setError(null);
		passwordTextView.setError(null);
		rePasswordTextView.setError(null);
		phoneTextView.setError(null);

		String name = nameTextView.getText().toString();
		String email = emailTextView.getText().toString();
		String password = passwordTextView.getText().toString();
		String repassword = rePasswordTextView.getText().toString();
		String phone = phoneTextView.getText().toString();
		
		boolean cancelLogin = false;
		View focusView = null;

		if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
			passwordTextView.setError(getString(R.string.invalid_password));
			focusView = passwordTextView;
			cancelLogin = true;
		}

		if (TextUtils.isEmpty(email)) {
			emailTextView.setError(getString(R.string.field_required));
			focusView = emailTextView;
			cancelLogin = true;
		} else if (!isEmailValid(email)) {
			emailTextView.setError(getString(R.string.invalid_email));
			focusView = emailTextView;
			cancelLogin = true;
		}

		if (cancelLogin) {
			// error in login
			focusView.requestFocus();
		} else {
			// show progress spinner, and start background task to login
			showProgress(true);
			userSigninTask = new UserSigninTask(name,email, password,repassword,phone);
			userSigninTask.execute((Void) null);
		}
	}

	private boolean isEmailValid(String email) {
		//add your own logic
		return email.contains("@");
	}

	private boolean isPasswordValid(String password) {
		//add your own logic
		return password.length() > 4;
	}


	public class UserSigninTask extends AsyncTask<Void, Void, Boolean> 
	{

		private final String nameStr;
		private final String emailStr;
		private final String passwordStr;
		private final String rePasswordStr;
		private final String contactStr;

		UserSigninTask(String name,String email, String password, String repassword,String contact) {
			nameStr = name;
			emailStr = email;
			passwordStr = password;
			rePasswordStr = repassword;
			contactStr = contact;
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			//this is where you should write your authentication code
			// or call external service
			// following try-catch just simulates network access
			try {
				Thread.sleep(1000);
				//String urlParameters = "email=" + emailStr + "&" + "pwd1=" + passwordStr;
				//   loginController.excutePost(urlParameters);

				List <NameValuePair> nvps = new ArrayList <NameValuePair>();
				nvps.add(new BasicNameValuePair("username", nameStr));
				nvps.add(new BasicNameValuePair("email", emailStr));
				nvps.add(new BasicNameValuePair("pwd1", passwordStr));
				nvps.add(new BasicNameValuePair("pwd2", rePasswordStr));
				nvps.add(new BasicNameValuePair("contact", contactStr));

				try {
					signinController.fetchCookieSignin(nvps);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}             
			} catch (InterruptedException e) 
			{
				return false;
			}
			return true;

		}

		@Override
		protected void onPostExecute(final Boolean success) {
			userSigninTask = null;
			//stop the progress spinner
			showProgress(false);

			if (success) {
				//  login success and move to main Activity here.
				AlertDialog.Builder builder = new AlertDialog.Builder(SigninAcitvity.this);  
				//Uncomment the below code to Set the message and title from the strings.xml file  
				//builder.setMessage(R.string.dialog_message) .setTitle(R.string.dialog_title);  

				//Setting message manually and performing action on button click  
				builder.setMessage("Profile created successfully.Please login to conitnue.")  
				.setCancelable(false)  
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {  
					public void onClick(DialogInterface dialog, int id) {  
						Intent intent = new Intent(getApplicationContext(),
								LoginActivity.class);
						if (intent != null) {
							intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
							getApplicationContext().startActivity(intent);
						}
						dialog.cancel();  
						finish();
					}  
				});  

				//Creating dialog box  
				AlertDialog alert = builder.create();  
				//Setting the title manually  
				alert.setTitle("Account Created successfully.");  
				alert.show();  
			} else {
				// login failure
				Toast.makeText(SigninAcitvity.this,"Incorrect Details. Unable to create profile.", Toast.LENGTH_LONG);
			}
			
		}

		@Override
		protected void onCancelled() {
			userSigninTask = null;
			showProgress(false);
		}
	}


    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<String>();
        cursor.moveToFirst();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

}
