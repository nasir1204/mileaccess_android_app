package com.mileaccess.consumer.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.inputmethod.InputMethodManager;


public class BaseFragment extends Fragment {
	
	protected Context context;
	protected ProgressDialog progressDialog;
	
	@Override
	public void onResume() {
		super.onResume();
	}

	public void hideKeyboard(Activity activity) {   
	    // Check if no view has focus:
	    View view = activity.getCurrentFocus();
	    if (view != null) {
	        InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
	        inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	    }
	}
}
