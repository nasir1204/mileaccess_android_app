package com.mileaccess.consumer.utilities;  

import java.util.ArrayList;  
import java.util.List;  
   

import android.content.ContentValues;  
import android.content.Context;  
import android.database.Cursor;  
import android.database.sqlite.SQLiteDatabase;  
import android.database.sqlite.SQLiteOpenHelper;  
import android.widget.Toast;
   
public class DatabaseHandler extends SQLiteOpenHelper {  
   private static final int DATABASE_VERSION = 1;  
   private static final String DATABASE_NAME = "orderManager";  
    private static final String TABLE_ORDERS = "orders";  
    private static final String KEY_CRN = "crn";  
    private static final String KEY_ORDER_DETAILS = "order_details";  
    private Context mContext;
   
    public DatabaseHandler(Context context) {  
        super(context, DATABASE_NAME, null, DATABASE_VERSION);  
        //3rd argument to be passed is CursorFactory instance  
        mContext = context;
    }  
   
    // Creating Tables  
    @Override  
    public void onCreate(SQLiteDatabase db) {  
    	db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_ORDERS + " ( " + 
    	//		"orders_id" + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " + 
    			KEY_CRN + " TEXT , " + 
    			KEY_ORDER_DETAILS + " TEXT );" 
    	        );
    }  
   
    // Upgrading database  
    @Override  
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {  
        // Drop older table if existed  
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDERS);  
   
        // Create tables again  
        onCreate(db);  
    }  
   
     // code to add the new contact  
     public void addOrderDetails(String crn,String orderDetails) {  
        SQLiteDatabase db = this.getWritableDatabase();  
   
        ContentValues values = new ContentValues();  
        values.put(KEY_CRN, crn); // Contact Name  
        values.put(KEY_ORDER_DETAILS, orderDetails); // Contact Phone  
   
        // Inserting Row  
        long value = db.insert(TABLE_ORDERS, null, values);
        Toast.makeText(mContext, "Inserted Value = "+String.valueOf(value) , Toast.LENGTH_SHORT).show();
        //2nd argument is String containing nullColumnHack  
        db.close(); // Closing database connection  
    }  
   
    // code to get the single contact  
    public String getOrderDetails(String crn) {  
        String orderDetails = null;
    	SQLiteDatabase db = this.getReadableDatabase();  
        // return ordersetails  
        String selectQuery = "SELECT "+ KEY_ORDER_DETAILS +" FROM "+ TABLE_ORDERS + " WHERE "+KEY_CRN+"=?";
        Cursor c = db.rawQuery(selectQuery, new String[] {crn});
        if (c!=null && c.getCount()>0) {
        	c.moveToFirst();
        	orderDetails = c.getString(c.getColumnIndexOrThrow(KEY_ORDER_DETAILS));
            c.close();
        }
 
        return orderDetails;  
    }  
   
    // code to get all contacts in a list view  
    public List getAllContacts() {  
        ArrayList<String> array_list = new ArrayList<String>();
        ArrayList<ArrayList<String>> listOfLists = new ArrayList<ArrayList<String>>();

        // Select All Query  
        String selectQuery = "SELECT  * FROM " + TABLE_ORDERS;  
   
        SQLiteDatabase db = this.getWritableDatabase();  
        Cursor cursor = db.rawQuery(selectQuery, null);  
   
        // looping through all rows and adding to list  
//        if (cursor.moveToFirst()) {  
//            do {  
//                Contact contact = new Contact();  
//                contact.setID(Integer.parseInt(cursor.getString(0)));  
//                contact.setName(cursor.getString(1));  
//                contact.setPhoneNumber(cursor.getString(2));  
//                // Adding contact to list  
//                array_list.add(contact);  
//            } while (cursor.moveToNext());  
//        }  
   
        // return contact list  
        return array_list;  
    }  
   
    // code to update the single contact  
    public int updateContact(String crn, String orderDetails) {  
        SQLiteDatabase db = this.getWritableDatabase();  
   
        ContentValues values = new ContentValues();  
        values.put(KEY_CRN, crn);  
        values.put(KEY_ORDER_DETAILS, orderDetails);  
   
        // updating row  
        return db.update(TABLE_ORDERS, values, KEY_CRN + " = ?",  
                new String[] {crn});  
    }  
   
    // Deleting single contact  
    public void deleteContact(String crn) {  
        SQLiteDatabase db = this.getWritableDatabase();  
        String[] crnArray = new String[] {crn};
        db.delete(TABLE_ORDERS, KEY_CRN + " = ?",  crnArray);  
        db.close();  
    }  
    
    // Deleting single contact  
    public void deleteAllContact() {  
        SQLiteDatabase db = this.getWritableDatabase();  
        db.delete(TABLE_ORDERS, null,  null);  
        db.close();  
    } 
   
    // Getting Orders Count  
    public int getOrdersCount() {  
        String countQuery = "SELECT  * FROM " + TABLE_ORDERS;  
        SQLiteDatabase db = this.getReadableDatabase();  
        Cursor cursor = db.rawQuery(countQuery, null);  
        cursor.close();  
   
        // return count  
        return cursor.getCount();  
    }  
   
}  